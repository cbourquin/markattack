package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class TestDatabase {
    private static String DATABASE_URL = "jdbc:mysql://172.20.128.68:3306/m2test1-test";
    private static String DATABASE_LOGIN = "m2test1";
    private static String DATABASE_PASSWORD = "m2test1";
    private Connection connection;
    private Properties properties;


    public static final TestDatabase TEST_DATABASE = new TestDatabase();

    private TestDatabase() {
        this.properties = initializeTestDataBaseConnectionProperties();
        setDataBaseDriver();
    }

    Connection getConnection() {
        return connection;
    }

    void exec(String query) throws Exception {
        Statement stmt = connection.createStatement();
        stmt.execute(query);
    }

    public void disconnect(){
        try {
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void connect() {
        try {
            connection = DriverManager.getConnection(DATABASE_URL, properties);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void setDataBaseDriver() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Properties initializeTestDataBaseConnectionProperties() {
        Properties properties = new Properties();
        properties.put("user", DATABASE_LOGIN);
        properties.put("password", DATABASE_PASSWORD);
        return properties;
    }
}