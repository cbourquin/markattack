package database;

final class SQLStatements {
    static final String INSERT_PERSON = "INSERT INTO `Person`(`PK_Person`, `FK_Person_Type`, `Person_Lastname`, `Person_Firstname`, `Person_Login`, `Person_Password`, `Person_Mail`) VALUES (''{0}'', ''{1}'', ''{2}'', ''{3}'', ''{4}'', ''{5}'', ''{6}'');";
    static final String INSERT_MODULE = "INSERT INTO `Module`(`PK_Module`, `Module_Name`, `Module_Coefficient`) VALUES (''{0}'',''{1}'',''{2}'');";
    static final String INSERT_MODULE_PERSON = "INSERT INTO `Module_Person`(`FK_Module`, `FK_Person`) VALUES (''{0}'',''{1}'');";
    static final String INSERT_GRADE = "INSERT INTO `grade`(`FK_Module`, `FK_Student`, `Grade_Value`) VALUES (''{0}'',''{1}'',''{2}'');";
    public static String REMOVE_MODULE_PERSON="DELETE FROM `Module_Person` WHERE `FK_Module`=''{0}'' AND `FK_Person`=''{1}''";
}
