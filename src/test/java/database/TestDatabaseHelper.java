package database;

import com.ibatis.common.jdbc.ScriptRunner;

import java.io.*;

import static database.PersonType.*;
import static database.SQLStatements.*;
import static database.TestDatabase.TEST_DATABASE;
import static java.text.MessageFormat.format;


public final class TestDatabaseHelper {

    public static void initializeTestDataBase() throws IOException {
        executeScript(TestDatabaseHelper.class.getResourceAsStream("initTestDatabase.sql"));
    }

    public static void cleanTestDataBase() throws IOException {
        executeScript(TestDatabaseHelper.class.getResourceAsStream("cleanTestDatabase.sql"));
    }

    public static void insertTeacher(int id, String lastName, String firstName, String login, String password, String mail) throws Exception {
        TEST_DATABASE.exec(format(INSERT_PERSON, id, TEACHER, lastName, firstName, login, password, mail));
    }

    public static void insertStudent(int id, String lastName, String firstName, String login, String password, String mail) throws Exception {
        TEST_DATABASE.exec(format(INSERT_PERSON, id, STUDENT, lastName, firstName, login, password, mail));
    }

    public static void insertAdmin(int id, String lastName, String firstName, String login, String password, String mail) throws Exception {
        TEST_DATABASE.exec(format(INSERT_PERSON, id, ADMIN, lastName, firstName, login, password, mail));
    }

    public static void insertModule(int id, String name, float coefficient) throws Exception {
        TEST_DATABASE.exec(format(INSERT_MODULE, id, name, coefficient));
    }

    public static void insertGrade(int moduleId, int studentId, float gradeValue) throws Exception {
        TEST_DATABASE.exec(format(INSERT_GRADE, moduleId, studentId, gradeValue));
    }

    public static void enrolPersonToModule(int moduleId, int personId) throws Exception {
        TEST_DATABASE.exec(format(INSERT_MODULE_PERSON, moduleId, personId));
    }

    public static void removePersonFromModule(int moduleId, int personId) throws Exception {
        TEST_DATABASE.exec(format(REMOVE_MODULE_PERSON, moduleId, personId));
    }

    private static void executeScript(final InputStream resource) throws IOException {
        Reader reader = null;
        InputStreamReader streamReader = null;
        try {
            ScriptRunner scriptExecutor = new ScriptRunner(TEST_DATABASE.getConnection(), false, false);
            streamReader = new InputStreamReader(resource);
            reader = new BufferedReader(streamReader);
            scriptExecutor.runScript(reader);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (streamReader != null) {
                streamReader.close();
            }
            if (reader != null) {
                reader.close();
            }
        }
    }
}