package automationTests;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public final class AutomationTestCaseHelper {

    public static boolean elementExists(HtmlUnitDriver driver, By locator) {
        try {
            driver.findElement(locator);
            return true;
        } catch (NoSuchElementException ex) {
            return false;
        }
    }

    public static TypeSafeMatcher<String> aPage(final String page) {
        return new TypeSafeMatcher<String>() {
            public void describeTo(Description description) {
                description.appendText("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test1/preprod/index.php?url="+page + " doesn't corresponds");
            }

            protected boolean matchesSafely(String item) {
                return item.equals("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test1/preprod/index.php?url=" + page);
            }
        };
    }

    public static String linkFollowingText(final String followedText, final String value) {
        return "(.//*[normalize-space(text()) and normalize-space(.)='" + followedText + "'])[1]/following::a["+value+"]";
    }
}
