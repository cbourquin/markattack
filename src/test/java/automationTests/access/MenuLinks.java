package automationTests.access;

public class MenuLinks {

    public static String HOME="home";
    public static String REPORTING_MODULE="reporting-subject-{0}";
    public static String REPORTING_STUDENT="reporting-student-{0}";
    public static String MODULE_SETTINGS_TEACHER="settings-teacher-subject-{0}";
    public static String MODULE_SETTINGS_MODULE="settings-student-subject-{0}";
    public static String MODULE_SETTINGS_GRADES="initGrades";
    public static String ADMINISTRATOR_SETTINGS_USER="settings-user-{0}";
    public static String ADMINISTRATOR_SETTINGS_MODULE ="settings-subject-{0}";

}
