package automationTests.access;

import automationTests.AutomationTestCase;
import database.TestDatabaseHelper;
import org.junit.Test;
import org.openqa.selenium.By;

import static automationTests.AutomationTestCaseHelper.aPage;
import static automationTests.AutomationTestCaseHelper.elementExists;
import static automationTests.AutomationTestCaseHelper.linkFollowingText;
import static automationTests.access.MenuLinks.*;
import static database.TestDatabaseHelper.*;
import static org.junit.Assert.*;
import static org.openqa.selenium.By.linkText;

public class AccessRightsTest extends AutomationTestCase {

    @Test
    public void studentAccessRights() throws Exception {
        connect("student", "test");
        assertTrue(elementExists(driver, linkText("Student Student")));

        assertTrue(elementExists(driver, linkText("Accueil")));

        assertFalse(elementExists(driver, linkText("Reporting")));
        assertFalse(elementExists(driver, linkText("Configuration")));

        verifyPageInaccessibility(REPORTING_MODULE, "1", "2", "id");
        verifyPageInaccessibility(REPORTING_STUDENT, "1", "2", "id");
        verifyPageInaccessibility(MODULE_SETTINGS_TEACHER, "1", "2", "id");
        verifyPageInaccessibility(MODULE_SETTINGS_MODULE, "1", "2", "id");
        verifyPageInaccessibility(MODULE_SETTINGS_GRADES);
        verifyPageInaccessibility(ADMINISTRATOR_SETTINGS_MODULE, "1", "2", "subject");
        verifyPageInaccessibility(ADMINISTRATOR_SETTINGS_USER, "1", "2", "user");
    }


    @Test
    public void teacherAccessRightsWithModule() throws Exception {
        connect("teacher", "test");
        assertTrue(elementExists(driver, linkText("Teacher Teacher")));

        openPageByMenu("Accueil");
        assertThat(driver.getCurrentUrl(), aPage("home"));

        openPageByMenu("Reporting", "1");
        assertThat(driver.getCurrentUrl(), aPage("reporting-subject-%7Bid%7D"));
        assertEquals("Reporting par module", currentPageTitle());

        openPageByMenu("Reporting", "2");
        assertThat(driver.getCurrentUrl(), aPage("reporting-student-%7Bid%7D"));
        assertEquals("Reporting par étudiant", currentPageTitle());

        openPageByMenu("Configuration", "1");
        assertThat(driver.getCurrentUrl(), aPage("settings-teacher-subject-%7Bid%7D"));
        assertEquals("Gestion d'un module", currentPageTitle());

        openPageByMenu("Configuration", "2");
        assertThat(driver.getCurrentUrl(), aPage("settings-student-subject-%7Bid%7D"));
        assertEquals("Gestion d'un module", currentPageTitle());

        openPageByMenu("Configuration", "3");
        assertThat(driver.getCurrentUrl(), aPage("grades"));
        assertEquals("Notes du module Test", currentPageTitle());


        openPageByMenu("Configuration", "4");
        assertThat(driver.getCurrentUrl(), aPage("settings-subject-%7Bsubject%7D"));
        assertEquals("Gestion des modules", currentPageTitle());

        assertFalse(elementExists(driver, By.linkText("Utilisateur")));
        verifyPageInaccessibility(ADMINISTRATOR_SETTINGS_USER, "1", "2", "user");
    }

    @Test
    public void teacherAccessRightsWithoutModule() throws Exception {
        removePersonFromModule(1, 2);
        connect("teacher", "test");
        assertTrue(elementExists(driver, linkText("Teacher Teacher")));
        removePersonFromModule(1, 2);


        openPageByMenu("Configuration", "1");
        assertThat(driver.getCurrentUrl(), aPage("settings-teacher-subject-%7Bid%7D"));
        assertEquals("Information", currentPageTitle());

        openPageByMenu("Configuration", "2");
        assertThat(driver.getCurrentUrl(), aPage("settings-student-subject-%7Bid%7D"));
        assertEquals("Information", currentPageTitle());

        openPageByMenu("Configuration", "3");
        assertThat(driver.getCurrentUrl(), aPage("grades"));
        assertEquals("Vous avez aucun module !", currentPageTitle());
    }

    @Test
    public void adminAccessRights() throws Exception {
        connect("admin", "test");
        assertThat(driver.getCurrentUrl(), aPage("home"));

        openPageByMenu("Reporting", "1");
        assertThat(driver.getCurrentUrl(), aPage("reporting-subject-%7Bid%7D"));
        assertEquals("Reporting par module", currentPageTitle());

        openPageByMenu("Reporting", "2");
        assertThat(driver.getCurrentUrl(), aPage("reporting-student-%7Bid%7D"));
        assertEquals("Reporting par étudiant", currentPageTitle());

        openPageByMenu("Configuration", "1");
        assertThat(driver.getCurrentUrl(), aPage("settings-teacher-subject-%7Bid%7D"));
        assertEquals("Gestion d'un module", currentPageTitle());

        openPageByMenu("Configuration", "2");
        assertThat(driver.getCurrentUrl(), aPage("settings-student-subject-%7Bid%7D"));
        assertEquals("Gestion d'un module", currentPageTitle());

        openPageByMenu("Configuration", "3");
        assertThat(driver.getCurrentUrl(), aPage("grades"));
        assertEquals("Vous avez aucun module !", currentPageTitle());


        openPageByMenu("Configuration", "4");
        assertThat(driver.getCurrentUrl(), aPage("settings-user-%7Buser%7D"));
        assertEquals("Gestion des utilisateurs", currentPageTitle());

        openPageByMenu("Configuration", "5");
        assertThat(driver.getCurrentUrl(), aPage("settings-subject-%7Bsubject%7D"));
        assertEquals("Gestion des modules", currentPageTitle());
    }


    protected void fillTestData() throws Exception {
        insertStudent(1, "Student", "Student", "student", "test", "");
        insertTeacher(2, "Teacher", "Teacher", "teacher", "test", "");
        insertAdmin(3, "Admin", "Admin", "admin", "test", "");

        insertModule(1, "Test", 1.5f);
        insertModule(2, "English", 1.5f);

        enrolPersonToModule(1, 2);

    }




}
