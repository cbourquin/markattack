package automationTests;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.Timeout;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import static automationTests.AutomationTestCaseHelper.linkFollowingText;
import static database.TestDatabase.TEST_DATABASE;
import static database.TestDatabaseHelper.cleanTestDataBase;
import static database.TestDatabaseHelper.initializeTestDataBase;
import static java.lang.String.format;
import static java.lang.reflect.Array.get;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.openqa.selenium.By.linkText;
import static org.openqa.selenium.By.xpath;

public abstract class AutomationTestCase {
    protected HtmlUnitDriver driver;
    protected StringBuffer verificationErrors = new StringBuffer();

//    @Rule
//    public Timeout globalTimeout = Timeout.seconds(30000);

    @Before
    public void setUp() throws Exception {
        driver = new HtmlUnitDriver(true) {
            @Override
            protected WebClient newWebClient(BrowserVersion version) {
                WebClient webClient = super.newWebClient(version);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                return webClient;
            }
        };
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        TEST_DATABASE.connect();
        initializeTestDataBase();
        fillTestData();
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
        cleanTestDataBase();
        TEST_DATABASE.disconnect();
    }

    protected abstract void fillTestData() throws Exception;


    public void connect(String login, String password) {
        goToPage("index");
        driver.findElement(By.id("pseudo")).clear();
        driver.findElement(By.id("pseudo")).sendKeys(login);
        driver.findElement(By.id("pwd")).clear();
        driver.findElement(By.id("pwd")).sendKeys(password);
        driver.findElement(xpath("(.//*[normalize-space(text()) and normalize-space(.)='Mot de passe:'])[1]/following::button[1]")).click();
    }

    private void goToPage(final String page) {
        driver.get("http://m2gl.deptinfo-st.univ-fcomte.fr/~m2test1/preprod/index.php?url=" + page);
    }

    private void verifyPageInaccessibility(final String page) {
        goToPage(page);
        assertEquals("Erreur 400: Accés non autorisée", driver.findElement(xpath("(.//*[normalize-space(text()) and normalize-space(.)='Deconnexion'])[1]/following::h3[1]")).getText());
    }

    protected void verifyPageInaccessibility(final String pageTemplate, String... ids) {
        for (String id : ids) {
            verifyPageInaccessibility(format(pageTemplate, id));
        }
    }

    protected void openPageByMenu(String... menus) {
        driver.findElement(linkText((String) get(menus, 0))).click();
        for (int i = 1; i < menus.length; i++) {
            WebElement element = driver.findElement(xpath(linkFollowingText(menus[i - 1], menus[i])));
            element.click();
        }
    }

    protected String currentPageTitle() {
        return driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Deconnexion'])[1]/following::h2[1]")).getText();
    }


}
