package automationTests;

import org.junit.Test;
import org.openqa.selenium.By;

import static database.TestDatabaseHelper.insertStudent;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
public class AuthenticationTest extends AutomationTestCase {

    @Test (timeout = 30000)
    public void authentication() throws Exception {
        connect("cbourquin", "test");
        assertEquals("Clement Bourquin", driver.findElement(By.linkText("Clement Bourquin")).getText());
    }


    protected void fillTestData() throws Exception {
        insertStudent(1, "Bourquin", "Clement", "cbourquin", "test", "");
    }

}
