
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

DELETE FROM `Grade`;
-- DROP TABLE IF EXISTS `Grade`;

DELETE FROM `Module_Person`;
-- DROP TABLE IF EXISTS `Module_Person`;

DELETE FROM `Module`;
-- DROP TABLE IF EXISTS `Module`;

DELETE FROM `Person`;
-- DROP TABLE IF EXISTS `Person`;

DELETE FROM `Person_Type`;
-- DROP TABLE IF EXISTS `Person_Type`;