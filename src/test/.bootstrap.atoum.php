<?php

// Autoloader.
require __DIR__ .
    DIRECTORY_SEPARATOR .
    '..' .
    DIRECTORY_SEPARATOR .
    'main' .
    DIRECTORY_SEPARATOR .
    '.autoloader.atoum.php';

// Test.php
require __DIR__ .
    DIRECTORY_SEPARATOR .
    'Test.php';
