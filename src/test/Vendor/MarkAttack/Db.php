<?php
/**
 * Created by PhpStorm.
 * User: juju3
 * Date: 09/10/2018
 * Time: 13:48
 */

namespace MarkAttack\dao\tests\units {

    use \atoum;
    use MarkAttack\entity\Person;
    use MarkAttack\dao\Db as DbSrc;

    class Db extends atoum
    {
        private $dbInstance;

        public function setUp()
        {
            $this->dbInstance = new DbSrc();
            $sql = file_get_contents( dirname(__FILE__) . '/Resources/createTestDataBase.sql');
            $this->dbInstance->query($sql);
        }

        function testConnection()
        {

            $db = new DbSrc('m2test1-test-tony');
            $conn = $db->getConnection();

            $dbname = $db->getValue('SELECT DATABASE() FROM DUAL');
            $this
                ->object($conn)
                ->isNotEqualTo(null);
            $this
                ->String($dbname)
                ->isEqualTo("m2test1-test-tony");
        }


        function testInsert(){
            $db = new DbSrc('m2test1-test-tony');
            $values = array('Person_Login'=>"test","Person_Firstname"=>"Julien","Person_Lastname"=>"Vannier","Person_Mail"=>"julien.vannier@gmail.com",'FK_Person_Type'=>1);
            $db->insert('Person',$values,true);
            $res = $db->getRow("SELECT Person_Firstname,Person_Login,Person_Lastname,Person_Mail,FK_Person_Type FROM Person");
            $this
                ->array($res)
                ->isNotEmpty();
            $this
                ->array($res)
                ->isEqualTo($values);
        }

        function testQueryWithoutParam(){
            $db = new DbSrc('m2test1-test-tony');
            $sql = 'SELECT Person_Firstname FROM Person';
            $name = $db->query($sql);
            $this
                ->String($name[0]['Person_Firstname'])
                ->isEqualTo("Julien");
        }

        function testUpdate(){
            $db = new DbSrc('m2test1-test-tony');
            $values = array("Person_Firstname"=>"Julien","Person_Lastname"=>'Vannier',"Person_Mail"=>'julien@gmail.com','FK_Person_Type'=>"1");
            $db->update('Person',$values,'PK_Person = 1',true);
            $res = $db->getRow("SELECT Person_Firstname,Person_Lastname,Person_Mail,FK_Person_Type FROM Person");
            $this
                ->array($res)
                ->isNotEmpty();
            $this
                ->array($res)
                ->isEqualTo($values);
        }


        public function testGetValue(){
            $sql = 'SELECT Person_Firstname FROM Person';
            $db = new DbSrc('m2test1-test-tony');
            $name = $db->getValue($sql);
            $this
                ->String($name)
                ->isEqualTo("Julien");
        }

        public function testDelete(){
            $db = new DbSrc('m2test1-test-tony');
            $res = $db->delete('Person',array('PK_Person' => 1));
            $this
                ->boolean($res)
                ->isTrue();
        }

        public function testDeleteFail(){
            $db = new DbSrc('m2test1-test-tony');
            $res = $db->delete('Person',array('PK_Person' => 1));
            $this
                ->boolean($res)
                ->isFalse();
        }


        public function tearDown()
        {
            $db = new DbSrc();
            $db->query("DROP DATABASE m2test1-test-tony");
        }
    }
}