
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


CREATE DATABASE IF NOT EXISTS `m2test1-test-tony` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `m2test1-test-tony`;


DROP TABLE IF EXISTS `Grade`;
CREATE TABLE IF NOT EXISTS `Grade` (
`PK_Grade` int(11) NOT NULL,
  `FK_Module` int(11) NOT NULL,
  `FK_Student` int(11) NOT NULL,
  `Grade_Value` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `Module_Person`;
CREATE TABLE IF NOT EXISTS `Module_Person` (
  `FK_Person` int(11) NOT NULL,
  `FK_Module` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `Module`;
CREATE TABLE IF NOT EXISTS `Module` (
`PK_Module` int(11) NOT NULL,
  `Module_Name` varchar(30) NOT NULL,
  `Module_Coefficient` decimal(3,1) NOT NULL DEFAULT '1.0'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `Person`;
CREATE TABLE IF NOT EXISTS `Person` (
`PK_Person` int(11) NOT NULL,
  `FK_Person_Type` int(11) NOT NULL,
  `Person_Lastname` varchar(50) NOT NULL,
  `Person_Firstname` varchar(50) NOT NULL,
  `Person_Mail` varchar(50) NOT NULL,
  `Person_Password` varchar(60) NOT NULL,
  `Person_Login` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;




DROP TABLE IF EXISTS `Person_Type`;
CREATE TABLE IF NOT EXISTS `Person_Type` (
`PK_Person_Type` int(11) NOT NULL,
  `Type_Name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=latin1;


INSERT INTO `Person_Type` (`PK_Person_Type`, `Type_Name`) VALUES
(1, 'Etudiant'),
(2, 'Enseignant'),
(3, 'Admin');


ALTER TABLE `Grade`
 ADD PRIMARY KEY (`PK_Grade`), ADD UNIQUE KEY `Unique_Grade` (`FK_Module`,`FK_Student`), ADD KEY `FK_Module` (`FK_Module`), ADD KEY `FK_Grade_Student` (`FK_Student`);

ALTER TABLE `Module`
 ADD PRIMARY KEY (`PK_Module`);

ALTER TABLE `Module_Person`
 ADD PRIMARY KEY (`FK_Module`,`FK_Person`), ADD KEY `FK_Module_Person_Person` (`FK_Person`);

ALTER TABLE `Person`
 ADD PRIMARY KEY (`PK_Person`), ADD UNIQUE KEY `Person_Login` (`Person_Login`), ADD KEY `FK_Person_Type` (`FK_Person_Type`);

ALTER TABLE `Person_Type`
 ADD PRIMARY KEY (`PK_Person_Type`);


ALTER TABLE `Grade`
MODIFY `PK_Grade` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;

ALTER TABLE `Module`
MODIFY `PK_Module` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;

ALTER TABLE `Person`
MODIFY `PK_Person` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;

ALTER TABLE `Person_Type`
MODIFY `PK_Person_Type` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;

ALTER TABLE `Grade`
ADD CONSTRAINT `FK_Grade_Student` FOREIGN KEY (`FK_Student`) REFERENCES `Person` (`PK_Person`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_Grade_Module` FOREIGN KEY (`FK_Module`) REFERENCES `Module` (`PK_Module`) ON DELETE CASCADE;

ALTER TABLE `Module_Person`
ADD CONSTRAINT `FK_Module_Person_Module` FOREIGN KEY (`FK_Module`) REFERENCES `Module` (`PK_Module`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_Module_Person_Person` FOREIGN KEY (`FK_Person`) REFERENCES `Person` (`PK_Person`) ON DELETE CASCADE;

ALTER TABLE `Person`
ADD CONSTRAINT `FK_Person_Type` FOREIGN KEY (`FK_Person_Type`) REFERENCES `Person_Type` (`PK_Person_Type`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
