<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 08/10/2018
 * Time: 21:14
 */

namespace MarkAttack\configuration\tests\units;

use MarkAttack\configuration\Route as srcRoute;
use atoum;

class Route extends atoum
{
    public function testMatch01(){
        $this
            ->given($route = new srcRoute('settings-add-subject-{id}', null))
            ->if($road = $route->match('settings-add-subject-12'))
            ->then
                ->boolean($road)
                ->isEqualTo(true)
            ;
    }

    public function testMatch02(){
        $this
            ->given($route = new srcRoute('settings-add-subject-{id}', null))
            ->if($road = $route->match('settings-add-subject-'))
            ->then
            ->boolean($road)
            ->isEqualTo(false)
        ;
    }

    public function testCall(){
        $this
            ->given($route = new srcRoute('settings-add-subject', function(){return 'oui';}))
            ->if($res = $route->call())
            ->then
                ->string($res)
                ->isEqualTo('oui')
        ;
    }


}