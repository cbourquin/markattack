<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 08/10/2018
 * Time: 21:14
 */

namespace MarkAttack\configuration\tests\units;

use MarkAttack\configuration\Router as srcRouter;
use MarkAttack\configuration\Route as srcRoute;
use atoum;

class Router extends atoum
{
    public function testAddPost(){
        $this
            ->given($router = new srcRouter(''))
            ->if($res = $router->post('settings-add-subject-{subject}', function () {
                return 'yes';
            }))
            ->then
                ->object($res)
                ->isInstanceOf(srcRoute::class)
                ->string($res->getPath())
                ->isEqualTo('settings-add-subject-{subject}')
                ->string($res->call())
                ->isEqualTo('yes')
        ;
    }

    public function testAddGet(){
        $this
            ->given($router = new srcRouter(''))
            ->if($res = $router->post('settings-add-subject-{subject}', function () {
                return 'yes';
            }))
            ->then
            ->object($res)
            ->isInstanceOf(srcRoute::class)
            ->string($res->getPath())
            ->isEqualTo('settings-add-subject-{subject}')
            ->string($res->call())
            ->isEqualTo('yes')
        ;
    }

    public function testRunGet(){
        $this
            ->given($_SERVER['REQUEST_METHOD'] = 'GET')
                ->and($router = new srcRouter('settings-add-subject-12'))
                ->and($router->get('settings-add-subject-{subject}', function ($id) {
                    return $id;
                }))
            ->if($res = $router->run())
            ->then
            ->string($res)
            ->isEqualTo('12')
        ;
    }

    public function testRunPost(){
        $this
            ->given($_SERVER['REQUEST_METHOD'] = 'POST')
            ->and($router = new srcRouter('settings-add-teacher-1-subject-12'))
            ->and($router->post('settings-add-teacher-{teacher}-subject-{subject}', function ($teacher, $subject) {
                return $teacher . $subject;
            }))
            ->if($res = $router->run())
            ->then
            ->string($res)
            ->isEqualTo('112')
        ;
    }


}