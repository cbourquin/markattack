<?php
/**
 * Created by PhpStorm.
 * User: juju3
 * Date: 08/10/2018
 * Time: 11:47
 */

namespace MarkAttack\entity\tests\units {

    use \atoum;
    use MarkAttack\entity\Grade;
    use MarkAttack\entity\Module;
    use MarkAttack\entity\Person;
    use MarkAttack\dao\Db;

    class ObjectModel extends atoum
    {
        private $dbInstance;
        public function setUp()
        {
            $this->dbInstance = new Db();
            $sql = file_get_contents( dirname(__FILE__) . '/Resources/createTestDataBase.sql');
            $this->dbInstance->query($sql);
        }
        public function testSaveUpdate(){
            $conn = new Db('m2test1-test-tony');
            $p = new Person(null,"test","test",1,"test@gmail.com","test","test");
            $p->setDb($conn);
            $p->save();
            $testname = "PPDA";
            $p->Person_Firstname = $testname;
            $p->save();
            $name = $conn->getValue("SELECT Person_Firstname FROM Person WHERE PK_Person = ".$p->id);
            $this
                ->string($name)
                ->isEqualTo($testname);
            $p->remove();
        }

        public function testRemove(){
            $conn = new Db('m2test1-test-tony');
            $p = new Person(null,"test","test",1,"test@gmail.com","test","test");
            $p->setDb($conn);
            $p->add();
            $p->remove();
            $res = $conn->query("SELECT * FROM Person WHERE PK_Person = ".$p->id) != false;
            $this
                ->boolean($res)
                ->isFalse();
        }

        public function testSaveInsert(){
            $conn = new Db('m2test1-test-tony');
            $p = new Person(null,"test","test",1,"test@gmail.com","test","test");
            $p->setDb($conn);
            $p->add();
            $res = $conn->query("SELECT * FROM Person WHERE PK_Person = ".$p->id) != false;
            $this
                ->boolean($res)
                ->isTrue();
            $p->remove();
        }

        public function testValidateInvalidMail(){
            $this
                ->given($p = new Person(null,'vannier','julien',1,'julien.vannier@gmail','jvannier','test'))
                ->then
                ->exception(
                    function () use ($p) {
                        $p->validateFields();
                    }
                )
                ->hasMessage('Person_Mail  -> L\'email de la personne n\'est pas valide');
        }

        public function testwrongtypeFloat(){
            $this
                ->given($g = new Grade(null,1, 1, "fdgdf"))
                ->then
                ->exception(
                    function () use ($g) {
                        $g->validateFields();
                    }
                )
                ->hasMessage("Grade_Value  -> La valeur n'est pas un nombre décimal");
        }

        public function testValidateInvalidLength(){
            $this
                ->given($p = new Person(null,'vannierrerzefezgfergergerverbreberbrebrebreberbebreberberberbreberberberbvannierrerzefezgfergergerverbreberbrebrebreberbebreberberberbreberberberb','julien',1,'julien.vannier@gmail.com','jvannier','test'))
                ->then
                ->exception(
                    function () use ($p) {
                        $p->validateFields();
                    }
                )
                ->hasMessage('Person_Lastname  -> Le nombre de caractères est limité à 99 caractères');
        }

        public function testValidateInvalidEmpty(){
            $this
                ->given($p = new Person(null,'','julien',1,'julien.vannier@gmail.com','jvannier','test'))
                ->then
                ->exception(
                    function () use ($p) {
                        $p->validateFields();
                    }
                )
                ->hasMessage('Person_Lastname  -> Champs vide');
        }

        public function testValidateInvalidMultipleErrors(){
            $this
                ->given($p = new Person(null,'','',1,'julien.vannier@gmail.com','jvannier','test'))
                ->then
                ->exception(
                    function () use ($p) {
                        $p->validateFields();
                    }
                )
                ->hasMessage('Person_Lastname  -> Champs vide, Person_Firstname  -> Champs vide');
        }

        public function testValidateGradeWithUnknownModule(){
            $p = new Person(null,"test","test",1,"test@gmail.com","testjulien","test");
            $p->setDb(new Db("m2test1-test-tony"));
            $p->save();
            $m = new Module(10);
            if($m)
                $m->remove();
            $this
                ->given($g = new Grade(null,$p->id,10,15.5))
                ->then
                ->exception(
                    function () use ($g) {
                        $g->validateFields();
                    }
                )
                ->hasMessage('FK_Module  -> Une des valeurs n\'éxiste pas Module');
            $p->remove();
        }


        public function testValidateGradeInvalidValue(){
            $conn = new Db("m2test1-test-tony");
            $p = new Person(null,"test","test",1,"test@gmail.com","testjulien","test");

            $p->setDb($conn);
            $p->save();
            $m = new Module(null,"test f",1);
            $m->setDb($conn);
            $m->save();
            $this
                ->given($g = new Grade(null,$p->id,$m->id,26))
                ->and($g->setDb($conn))
                ->then
                ->exception(
                    function () use ($g) {
                        $g->validateFields();
                    }
                )
                ->hasMessage('Grade_Value  -> La valeur saisie n\'est pas bonne : 26, elle doit être comprise entre 0 et 20');
            $p->remove();
            $m->remove();
        }

        public function testFormatField(){
            $value = \MarkAttack\entity\ObjectModel::formatValue("essaie",\MarkAttack\entity\ObjectModel::TYPE_STRING,true,false);
            $this
                ->String($value)
                ->isEqualTo("'essaie'");
        }

        public function testValidate(){
            $this
                ->given($p = new Person(null, 'vannier', 'julien', 1, 'julien.vannier@gmail.com', 'jvannier', 'test'))
                ->and($p->setDb(new Db('m2test1-test-tony')))
                ->when(
                    function() use ($p) {
                        $p->validateFields();
                    }
                )->error()
                ->notExists();
        }

        public function testGetDefinition(){
            $def = \MarkAttack\entity\ObjectModel::getDefinition('MarkAttack\entity\Person');
            $expected = array(
                'table' => 'Person',
                'primary' => 'PK_Person',
                'fields' => array(
                    /* Classic fields */
                    'Person_Lastname' => array('type' => \MarkAttack\entity\ObjectModel::TYPE_STRING),
                    'Person_Firstname' => array('type' => \MarkAttack\entity\ObjectModel::TYPE_STRING),
                    'FK_Person_Type' => array('type' => \MarkAttack\entity\ObjectModel::TYPE_INT,'validate' => 'isvalidtype'),
                    'Person_Mail' => array('type' => \MarkAttack\entity\ObjectModel::TYPE_STRING,'validate'=>'isvalidemail'),
                    'Person_Login' => array('type' => \MarkAttack\entity\ObjectModel::TYPE_STRING),
                    'Person_Password' => array('type'=>\MarkAttack\entity\ObjectModel::TYPE_PASSWORD),
                ),
                'classname'=> "MarkAttack\\entity\\Person",
            );
            $this->array($def)->isEqualTo($expected);
        }

        public function tearDown()
        {
            $db = new Db();
            $db->query("DROP DATABASE m2test1-test-tony");
        }
    }
}