<?php

namespace MarkAttack\frontend\configuration\tests\units {

    use atoum;
    use mageekguy\atoum\asserters\boolean;
    use mageekguy\atoum\test\data\set;
    use MarkAttack\entity\Module;
    use MarkAttack\entity\Person;
    use MarkAttack\dao\Db;
    use PhpParser\Node\Expr\AssignOp\Mod;

    class ActionConfiguration extends atoum
    {
        private $dbInstance;
        public function setUp()
        {
            $this->dbInstance = new \MarkAttack\dao\Db();
            $sql = file_get_contents( dirname(__FILE__) . '/../../Resources/createTestDataBase.sql');
            $this->dbInstance->query($sql);
        }

        function testConfigModules() {

            // id = 1 - TF
            $id = 1;
            $m = new Module($id);
            $list = $m->getStudent();
            $notList = $m->getNotStudent();

            $is_ok = true;

            $x = 0;

            foreach ($list as $p) {
                $x++;
                if (in_array($p, $notList)) {
                    $is_ok = false;
                } else {
                    $is_ok = true;
                }
            }

            $this->boolean($is_ok)->isTrue();

        }

        function testAjoutModule() {

            $db = new Db("m2test1-test-tony");

            $personne = new Person(null, "test", "test", 1, "ok@edu.fr", "login_test1", "test");
            $personne->setDb($db);
            $personne->save();

            $module = new Module(null, "module1", 45);
            $module->setDb($db);
            $module->save();

            if ($personne->isInModule($module->getId())) {
                $module->delPerson($personne);
            }

            $module->addPerson($personne);

            $this->string($personne->isInModule($module->getId()))->isEqualTo("1");

            $personne->remove();
            $module->remove();

        }

        function testSupprimeModule() {

            $db = new Db("m2test1-test-tony");

            $personne = new Person(null, "test", "test", 1, "ok@edu.fr", "login_test2", "test");
            $personne->setDb($db);
            $personne->save();

            $module = new Module(null, "module1", 45);
            $module->setDb($db);
            $module->save();

            $personne->addToModule($module->getId());

            $personne->removeToModule($module->getId());

            $this->boolean($personne->isInModule($module->getId()))->isFalse();

            $personne->remove();
            $module->remove();

        }

        function testAjoutProfModule() {

            $idModule = 1;
            $module = new Module($idModule);
            $teacherList = $module->getTeacher();
            $teacherNotList = $module->getTeacherNotList();

            $is_ok = true;

            foreach ($teacherList as $p) {
                if (in_array($p, $teacherNotList)) {
                    $is_ok = false;
                } else {
                    $is_ok = true;
                }
            }

            $this->boolean($is_ok)->IsTrue();

        }

        function testAjoutProf() {

            $db = new Db("m2test1-test-tony");

            $personne = new Person(null, "test", "test", 2, "ok@edu.fr", "login_test3", "test");
            $personne->setDb($db);
            $personne->save();

            $module = new Module(null, "module1", 45);
            $module->setDb($db);
            $module->save();

            if ($personne->isInModule($module->getId())) {
                $personne->removeToModule($module->getId());
            }

            $personne->addToModule($module->getId());

            $this->string($personne->isInModule($module->getId()))->isEqualTo("1");

            $personne->remove();
            $module->remove();

        }

        function testDeleteProf() {

            $db = new Db("m2test1-test-tony");

            $personne = new Person(null, "test", "test", 2, "ok@edu.fr", "login_test4", "test");
            $personne->setDb($db);
            $personne->save();

            $module = new Module(null, "module1", 45);
            $module->setDb($db);
            $module->save();

            if (!$personne->isInModule($module->getId())) {
                $personne->addToModule($module->getId());
            }

            $personne->removeToModule($module->getId());

            $this->boolean($personne->isInModule($module->getId()))->isFalse();

            $personne->remove();
            $module->remove();

        }

    }

}