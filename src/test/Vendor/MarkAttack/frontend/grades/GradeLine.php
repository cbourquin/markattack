<?php

namespace MarkAttack\frontend\grades\tests\units {

    use atoum;
    use mock\MarkAttack\entity\Grade;
    use mock\MarkAttack\entity\Module;
    use mock\MarkAttack\entity\ObjectFactory;
    use mock\MarkAttack\entity\Person;

    class   GradeLine extends atoum
    {


        function testRenderGrade()
        {
            $objectFactory = new ObjectFactory;

            $this->mockGenerator->orphanize('__construct');


            $this->given($gradeLine = self::createGradeLineHavingGradeValue($objectFactory, 17))
                ->then
                ->integer($gradeLine->gradeValue())
                ->isEqualTo(17);

            $this->given($gradeLine = self::createGradeLineHavingGradeValue($objectFactory, 13.5))
                ->then
                ->float($gradeLine->gradeValue())
                ->isEqualTo(13.5);
        }


        function testRenderPerson()
        {
            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory();
            $person = self::createPerson($objectFactory, null, "Smith", "Anna");
            $gradeLine = \MarkAttack\frontend\grades\GradeLine::createGradeLine($person, self::createGrade(), $objectFactory);

            $this->given($gradeLine)
                ->then
                ->string($gradeLine->firstName())
                ->isEqualTo("Anna");

            $this->given($gradeLine)
                ->then
                ->string($gradeLine->lastName())
                ->isEqualTo("Smith");
        }

        function testSaveGrade()
        {
            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory();
            $grade = self::createGrade($objectFactory);
            $gradeLine = \MarkAttack\frontend\grades\GradeLine::createGradeLine(self::createPerson(), $grade, $objectFactory);
            $this->mockGenerator->shunt('update');
            $this->mockGenerator->shunt('add');
            $gradeValue = 13.0;
            $this->given($gradeLine)
                ->if($gradeLine->save($gradeValue))
                ->then
                ->mock($grade)
                ->call('setGradeValue')
                ->withArguments($gradeValue)
                ->once()
                ->and
                ->mock($grade)
                ->call('save')
                ->once();
        }

        function testDontSaveSameGrade()
        {
            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory();
            $grade = self::createGrade($objectFactory, null, null, null, 13);
            $gradeLine = \MarkAttack\frontend\grades\GradeLine::createGradeLine(self::createPerson(), $grade, $objectFactory);

            $this->mockGenerator->shunt('update');
            $this->mockGenerator->shunt('add');
            $gradeValue = 13.0;
            $this->given($gradeLine)
                ->if($gradeLine->save($gradeValue))
                ->then
                ->mock($grade)
                ->call('setGradeValue')
                ->withArguments($gradeValue)
                ->never()
                ->and
                ->mock($grade)
                ->call('save')
                ->never();
        }

        function testGradeStyle()
        {
            $this->mockGenerator->orphanize('__construct');
            $this->mockGenerator->orphanize('update');
            $this->mockGenerator->orphanize('add');
            $objectFactory = new ObjectFactory();

            $this->given($gradeLine=self::createGradeLineHavingGradeValue($objectFactory, 13))
                ->then
                ->string($gradeLine->styleClassName())
                ->isEqualTo(\MarkAttack\frontend\grades\GradeLine::EXISTING_GRADE_STYLE);


            $this->given($gradeLine = self::createGradeLineHavingGradeValue($objectFactory, null))
                ->then
                ->string($gradeLine->styleClassName())
                ->isEqualTo(\MarkAttack\frontend\grades\GradeLine::NEW_GRADE_STYLE);

            $this->given($gradeLine = self::createGradeLineHavingGradeValue($objectFactory, "ABC"))
                ->then
                ->string($gradeLine->styleClassName())
                ->isEqualTo(\MarkAttack\frontend\grades\GradeLine::ERROR_GRADE_STYLE);

        }

        function testHasError(){
            $this->mockGenerator->orphanize('__construct');
            $this->mockGenerator->orphanize('update');
            $this->mockGenerator->orphanize('add');
            $objectFactory = new ObjectFactory();

            $this->given($gradeLine=self::createGradeLineHavingGradeValue($objectFactory, "ABS"))
                ->then
                ->boolean($gradeLine->hasWrongGradeValue())
                ->isTrue();


            $this->given($gradeLine=self::createGradeLineHavingGradeValue($objectFactory, null))
                ->then
                ->boolean($gradeLine->hasWrongGradeValue())
                ->isFalse();


            $this->given($gradeLine=self::createGradeLineHavingGradeValue($objectFactory, "13"))
                ->then
                ->boolean($gradeLine->hasWrongGradeValue())
                ->isFalse();
        }

        function testIsRequired(){
            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory();

            $this->given($gradeLine=self::createGradeLineHavingGradeValue($objectFactory, null))
                ->then
                ->string($gradeLine->isRequired())
                ->isEmpty();


            $this->given($gradeLine=\MarkAttack\frontend\grades\GradeLine::createGradeLine(
                $this->createPerson($objectFactory),
                $this->createGrade($objectFactory, 1), $objectFactory))
                ->then
                ->string($gradeLine->isRequired())
                ->contains("required");

        }
        /**********************************************
         * Utils
         **********************************************/
        private function createGradeLineHavingGradeValue($objectFactory, $gradeValue)
        {
            self::createGrade($objectFactory, null, null, null, $gradeValue);
            return \MarkAttack\frontend\grades\GradeLine::createExistingGradeLine(null, $objectFactory);
        }

        private function createGrade($objectFactory=null, $id = null, $idPerson = null, $idModule = null, $gradeValue = null)
        {
            $this->mockGenerator->orphanize('update');
            $this->mockGenerator->orphanize('add');
            $grade = new Grade;
            $this->calling($grade)->getId = $id;
            $this->calling($grade)->getFKStudent = $idPerson;
            $this->calling($grade)->getFKModule = $idModule;
            $this->calling($grade)->getGradeValue = $gradeValue;
            if (!($objectFactory==null)) {
                $this->calling($objectFactory)->createGrade = $grade;
            }
            return $grade;
        }

        private function createPerson($objectFactory=null,
                                      $id = null,
                                      $lastName = null,
                                      $firstName = null)
        {
            $person = new Person;
            $this->calling($person)->getId = $id;
            $this->calling($person)->getPersonLastname = $lastName;
            $this->calling($person)->getPersonFirstname = $firstName;
            if (!($objectFactory==null)) {
                $this->calling($objectFactory)->createPerson = $person;
            }
            return $person;
        }



    }


}