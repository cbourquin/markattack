<?php

namespace MarkAttack\frontend\grades\tests\units {

    use atoum;
    use mock\MarkAttack\entity\Module;
    use mock\MarkAttack\entity\ObjectFactory;

    class   GradesModel extends atoum
    {


        function testModuleGradesCreation()
        {
            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory();
            $module1 = $this->createModule(1, $objectFactory, 1, "Test Fontionnel");
            $module2 = $this->createModule(2, $objectFactory, 2, "Anglais");

            $moduleGradesManager = \MarkAttack\frontend\grades\GradesModel::createGradesModel(
                array(1, 2), $objectFactory);

            $this->given($moduleGradesManager)
                ->then
                ->array($moduleGradesManager->getModuleGrades())
                ->hasSize(2)
                ->object($moduleGradesManager->getModuleGrades()[0]->getModule())
                ->isIdenticalTo($module1)
                ->object($moduleGradesManager->getModuleGrades()[1]->getModule())
                ->isIdenticalTo($module2);


        }


        function testModuleGradesSelection()
        {
            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory();
            $module1 = $this->createModule(1, $objectFactory, 1, "Test Fontionnel");
            $module2 = $this->createModule(2, $objectFactory, 2, "Anglais");

            //Impossible retrieve selected moduleGrades, if there is nothing
            $this->given($moduleGradesManager = \MarkAttack\frontend\grades\GradesModel::
            createGradesModel(array(), $objectFactory))
                ->then
                ->exception(
                    function () use ($moduleGradesManager) {
                        $moduleGradesManager->retrieveSelectedModule();
                    }
                )
                ->hasMessage('There is no module !')
                ->exception(
                    function () use ($moduleGradesManager) {
                        $moduleGradesManager->retrieveSelectedModuleGradeLines();
                    }
                )
                ->hasMessage('There is no module !');

            $moduleGradesManager = \MarkAttack\frontend\grades\GradesModel::
            createGradesModel(array(1, 2), $objectFactory);
            $this->given($moduleGradesManager)
                ->then
                ->array($moduleGradesManager->getModuleGrades())
                ->hasSize(2)
                ->object($moduleGradesManager->retrieveSelectedModule())
                ->isIdenticalTo($module1)
                ->boolean($moduleGradesManager->isSelected($moduleGradesManager->getModuleGrades()[0]))
                ->isTrue()
                ->boolean($moduleGradesManager->isSelected($moduleGradesManager->getModuleGrades()[1]))
                ->isFalse();

            $this->given($moduleGradesManager)
                ->then
                ->array($moduleGradesManager->getModuleGrades())
                ->hasSize(2)
                ->if($moduleGradesManager->selectModuleGrades("2"))
                ->object($moduleGradesManager->retrieveSelectedModule())
                ->isIdenticalTo($module2)
                ->boolean($moduleGradesManager->isSelected($moduleGradesManager->getModuleGrades()[0]))
                ->isFalse()
                ->boolean($moduleGradesManager->isSelected($moduleGradesManager->getModuleGrades()[1]))
                ->isTrue();


        }


        private function createModule($callNumber = null,
                                      $objectFactory = null,
                                      $id = null,
                                      $moduleName = null)
        {

            $module = new Module;
            $this->calling($module)->getId = $id;
            $this->calling($module)->getModuleName = $moduleName;
            if (!($objectFactory == null)) {
                if ($callNumber == null) {
                    $this->calling($objectFactory)->createModule = $module;
                    return;
                }
                $this->calling($objectFactory)->createModule[$callNumber] = $module;
            }
            return $module;

        }
    }

}