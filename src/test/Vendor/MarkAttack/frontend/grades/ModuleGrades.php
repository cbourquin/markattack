<?php

namespace MarkAttack\frontend\grades\tests\units {

    use atoum;
    use MarkAttack\entity\ObjectFactory;
    use mock\MarkAttack\entity\Module;

    class   ModuleGrades extends atoum
    {


        function testGetGradeLines()
        {

            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory();
            $moduleGrades=new \mock\MarkAttack\frontend\grades\ModuleGrades(self::createModule(), $objectFactory);
            $moduleGradeLines = new \mock\MarkAttack\frontend\grades\ModuleGradeLines(self::createModule(), $objectFactory);

            $this->given($moduleGrades)
                ->and($this->calling($moduleGrades)->createModuleGradeLines = $moduleGradeLines)
                ->if($extractedModuleGradeLines = $moduleGrades->getModuleGradeLines())
                ->then
                ->object($extractedModuleGradeLines)
                ->isIdenticalTo($moduleGradeLines)
                ->mock($moduleGrades)
                ->call('createModuleGradeLines')
                ->once()
                ->if($extractedModuleGradeLines = $moduleGrades->getModuleGradeLines())
                ->then
                ->object($extractedModuleGradeLines)
                ->isIdenticalTo($moduleGradeLines)
                ->mock($moduleGrades)
                ->call('createModuleGradeLines')
                ->once();
        }

        private function createModule($objectFactory=null,
                                      $id = null,
                                      $moduleName = null)
        {
            $module = new Module();
            $this->calling($module)->getId = $id;
            $this->calling($module)->getModuleName = $moduleName;
            if (!($objectFactory==null)) {
                $this->calling($objectFactory)->createModule = $module;
            }
            return $module;

        }


    }

}