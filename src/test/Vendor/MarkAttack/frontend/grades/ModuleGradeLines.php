<?php

namespace MarkAttack\frontend\grades\tests\units {

    use atoum;
    use mock\MarkAttack\entity\Grade;
    use mock\MarkAttack\entity\Module;
    use mock\MarkAttack\entity\ObjectFactory;
    use mock\MarkAttack\entity\Person;

    class ModuleGradeLines extends atoum
    {
        function testFillGradeLines()
        {
            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory;
            $moduleGradeLines = new \mock\MarkAttack\frontend\grades\ModuleGradeLines(self::createModule(), $objectFactory);

            $grade1 = $this->createGrade(1, $objectFactory);
            $person1 = $this->createPerson(1, $objectFactory);
            $grade2 = $this->createGrade(2, $objectFactory);
            $grade3 = $this->createGrade(3, $objectFactory);
            $person2 = $this->createPerson(2, $objectFactory);
            $person3 = $this->createPerson(3, $objectFactory);

            $this
                ->given($moduleGradeLines)
                ->then
                //it's empty
                ->array($moduleGradeLines->getGradeLines())
                ->isEmpty()
                //insert only grade1 grades
                ->if($moduleGradeLines->fillExistingGradeLines(array(1, 2)))
                ->then
                ->array($moduleGradeLines->getGradeLines())
                ->hasSize(2)
                ->object($moduleGradeLines->getGradeLines()[0]->getGrade())
                ->isIdenticalTo($grade1)
                ->object($moduleGradeLines->getGradeLines()[0]->getPerson())
                ->isIdenticalTo($person1)
                ->object($moduleGradeLines->getGradeLines()[1]->getGrade())
                ->isIdenticalTo($grade2)
                ->object($moduleGradeLines->getGradeLines()[1]->getPerson())
                ->isIdenticalTo($person2)
                //insert new grades
                ->if($moduleGradeLines->fillNewGradeLines(array(3)))
                ->then
                ->array($moduleGradeLines->getGradeLines())
                ->hasSize(3)
                ->object($moduleGradeLines->getGradeLines()[0]->getGrade())
                ->isIdenticalTo($grade1)
                ->object($moduleGradeLines->getGradeLines()[0]->getPerson())
                ->isIdenticalTo($person1)
                ->object($moduleGradeLines->getGradeLines()[1]->getGrade())
                ->isIdenticalTo($grade2)
                ->object($moduleGradeLines->getGradeLines()[1]->getPerson())
                ->isIdenticalTo($person2)
                ->object($moduleGradeLines->getGradeLines()[2]->getGrade())
                ->isIdenticalTo($grade3)
                ->object($moduleGradeLines->getGradeLines()[2]->getPerson())
                ->isIdenticalTo($person3);


        }

        function testAddByVerifyingPersonId()
        {

            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory;
            $moduleGradeLines = new \mock\MarkAttack\frontend\grades\ModuleGradeLines(self::createModule(), $objectFactory);

            $existingGrade = $this->createGrade(null,$objectFactory);
            $existingPerson = $this->createPerson(null,  $objectFactory, 1);
            $newGrade = $this->createGrade(null, $objectFactory);
            $newPerson = $this->createPerson(null, $objectFactory);

            $moduleGradeLines->addGradeLine(\MarkAttack\frontend\grades\GradeLine::createGradeLine($existingPerson, $existingGrade, $objectFactory));
            $this
                ->given($moduleGradeLines)
                ->then
                ->array($moduleGradeLines->getGradeLines())
                ->hasSize(1)
                ->object($moduleGradeLines->getGradeLines()[0]->getGrade())
                ->isIdenticalTo($existingGrade)
                ->object($moduleGradeLines->getGradeLines()[0]->getPerson())
                ->isIdenticalTo($existingPerson)
                //impossible to insert as has same id
                ->if($moduleGradeLines->fillNewGradeLines(array(1)))
                ->then
                ->array($moduleGradeLines->getGradeLines())
                ->hasSize(1)
                ->object($moduleGradeLines->getGradeLines()[0]->getGrade())
                ->isIdenticalTo($existingGrade)
                ->object($moduleGradeLines->getGradeLines()[0]->getPerson())
                ->isIdenticalTo($existingPerson)
//                //insert as has different ids
                ->if($moduleGradeLines->fillNewGradeLines(array(3)))
                ->then
                ->array($moduleGradeLines->getGradeLines())
                ->hasSize(2)
                ->object($moduleGradeLines->getGradeLines()[0]->getGrade())
                ->isIdenticalTo($existingGrade)
                ->object($moduleGradeLines->getGradeLines()[0]->getPerson())
                ->isIdenticalTo($existingPerson)
                ->object($moduleGradeLines->getGradeLines()[1]->getGrade())
                ->isIdenticalTo($newGrade)
                ->object($moduleGradeLines->getGradeLines()[1]->getPerson())
                ->isIdenticalTo($newPerson);


        }

        function testHasError()
        {

            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory;
            $gradeLine1=new \mock\MarkAttack\frontend\grades\GradeLine;
            $gradeLine2=new \mock\MarkAttack\frontend\grades\GradeLine;
            $moduleGradeLines = new \mock\MarkAttack\frontend\grades\ModuleGradeLines(self::createModule(), $objectFactory);

            $moduleGradeLines->addGradeLine($gradeLine1);
            $moduleGradeLines->addGradeLine($gradeLine2);
            $this
                ->given($moduleGradeLines)
                ->and($this->calling($gradeLine1)->hasWrongGradeValue = false)
                ->and($this->calling($gradeLine2)->hasWrongGradeValue = false)
                ->then
                ->boolean($moduleGradeLines->hasGradeLineWithError())
                ->isFalse();

            $this
                ->given($moduleGradeLines)
                ->and($this->calling($gradeLine1)->hasWrongGradeValue = true)
                ->and($this->calling($gradeLine2)->hasWrongGradeValue = false)
                ->then
                ->boolean($moduleGradeLines->hasGradeLineWithError())
                ->isTrue();

        }

        function testSortGradeLines()
        {
            $this->mockGenerator->orphanize('__construct');
            $objectFactory = new ObjectFactory;
            $line1 = $this->createGradeLineHavingPerson($objectFactory,"Smith", "John");
            $line2 = $this->createGradeLineHavingPerson($objectFactory,"Smith", "Anne");
            $line3 = $this->createGradeLineHavingPerson($objectFactory,"Hanks", "Tom");
            $moduleGradeLines = new \mock\MarkAttack\frontend\grades\ModuleGradeLines(self::createModule(), $objectFactory);

            $this
                ->given($moduleGradeLines)
                ->if($moduleGradeLines->addGradeLine($line1))
                ->and($moduleGradeLines->addGradeLine($line2))
                ->and($moduleGradeLines->addGradeLine($line3))
                ->then
                ->array($moduleGradeLines->getGradeLines())
                ->isIdenticalTo(array($line1, $line2, $line3))
                ->if($moduleGradeLines->sortGradeLines())
                ->then
                ->array($moduleGradeLines->getGradeLines())
                ->isIdenticalTo(array($line3, $line2, $line1));

        }

        /**********************************************
         * Utils
         **********************************************/
        private function createGradeLineHavingPerson($objectFactory, $lastName, $firstName)
        {
            $person = self::createPerson(null, $objectFactory, null, $lastName, $firstName);
            $grade = self::createGrade($objectFactory);
            return \MarkAttack\frontend\grades\GradeLine::createGradeLine($person, $grade, $objectFactory);
        }

        private function createGrade($callNumber = null, $objectFactory = null, $id = null, $idPerson = null, $idModule = null, $gradeValue = null)
        {
            $this->mockGenerator->orphanize('update');
            $this->mockGenerator->orphanize('add');
            $grade = new Grade;
            $this->calling($grade)->getId = $id;
            $this->calling($grade)->getFKStudent = $idPerson;
            $this->calling($grade)->getFKModule = $idModule;
            $this->calling($grade)->getGradeValue = $gradeValue;
            if (!($objectFactory == null)) {
                if ($callNumber==null) {
                    //general call
                    $this->calling($objectFactory)->createGrade = $grade;
                    return $grade;
                }
                //specific number of call
                $this->calling($objectFactory)->createGrade[$callNumber] = $grade;
            }
            return $grade;
        }

        private function createPerson($callNumber = null,
                                      $objectFactory = null,
                                      $id = null,
                                      $lastName = null,
                                      $firstName = null)
        {
            $person = new Person;
            $this->calling($person)->getId = $id;
            $this->calling($person)->getPersonLastname = $lastName;
            $this->calling($person)->getPersonFirstname = $firstName;
            if (!($objectFactory == null)) {
                if ($callNumber==null) {
                    $this->calling($objectFactory)->createPerson = $person;
                    return $person;
                }
                $this->calling($objectFactory)->createPerson[$callNumber] = $person;
            }
            return $person;
        }

        private function createModule($objectFactory = null,
                                      $id = null,
                                      $moduleName = null)
        {
            $module = new Module();
            $this->calling($module)->getId = $id;
            $this->calling($module)->getModuleName = $moduleName;
            if (!($objectFactory == null)) {
                $this->calling($objectFactory)->createModule = $module;
            }
            return $module;

        }

    }

}