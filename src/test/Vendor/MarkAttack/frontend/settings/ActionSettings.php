<?php
/**
 * Created by IntelliJ IDEA.
 * User: arthu
 * Date: 16/10/2018
 * Time: 10:42
 */

namespace MarkAttack\frontend\settings\tests\units {


    use atoum;
    use MarkAttack\entity\Module;
    use MarkAttack\entity\Person;
    use MarkAttack\dao\Db;

    class ActionSettings extends atoum
    {

        private $dbInstance;
        public function setUp()
        {
            $this->dbInstance = new \MarkAttack\dao\Db();
            $sql = file_get_contents( dirname(__FILE__) . '/../../Resources/createTestDataBase.sql');
            $this->dbInstance->query($sql);
        }

        public function testAjouterUtilisateur(){

            $db = new Db("m2test1-test-tony");

           $personne = new Person(null, "test", "test", 1, "test@edu.fr", "test_ajout", "ok");
           $personne->setDb($db);
           $personne->save();

           $this->string($personne->getId())->isNotEqualTo(null);

           $personne->remove();

        }

        public function testAjoutModule() {

            $db = new Db("m2test1-test-tony");

            $module = new Module(null, "TestAjoutModule", 850);
            $module->setDb($db);
            $module->save();

            $this->string($module->getId())->isNotEqualTo(null);

            $module->remove();

        }

        public function testModifModule() {

            $db = new Db("m2test1-test-tony");

            $module = new Module(null, "TestModifModule", 450);
            $module->setDb($db);
            $module->save();

            $lastName = $module->Module_Name;
            $lastCoef = $module->Module_Coefficient;

            $this->string($lastName)->isEqualTo($module->Module_Name);
            $this->integer($lastCoef)->isEqualTo($module->Module_Coefficient);

            $module->remove();

        }

    }
}