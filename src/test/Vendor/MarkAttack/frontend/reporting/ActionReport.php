<?php
/**
 * Created by IntelliJ IDEA.
 * User: Tony
 * Date: 22/10/2018
 * Time: 16:33
 */

namespace MarkAttack\frontend\reporting\tests\units {

    use atoum;
    use \MarkAttack\frontend\reporting\ObjectValue;

    class ActionReport extends atoum {

        public function testGetMoyenne() {
            $this
                ->given($test = new ObjectValue(array('0'=>14.2, '1'=>8.4, '2'=>11.0, '3'=>7.54)))
                ->if($resultat = $test->getMoyenne())
                ->then
                    ->float($resultat)
                    ->isEqualTo(10.3);
        }

        public function testGetMinimum() {
            $this
                ->given($test = new ObjectValue(array('0'=>14.2, '1'=>8.4, '2'=>11.0, '3'=>7.54)))
                ->if($resultat = $test->getMin())
                ->then
                ->float($resultat)
                ->isEqualTo(7.5);
        }

        public function testGetMaximum() {
            $this
                ->given($test = new ObjectValue(array('0'=>14.2, '1'=>8.4, '2'=>11.0, '3'=>7.54)))
                ->if($resultat = $test->getMax())
                ->then
                ->float($resultat)
                ->isEqualTo(14.2);
        }

        public function testGetVariance() {
            $this
                ->given($test = new ObjectValue(array('0'=>14.2, '1'=>8.4, '2'=>11.0, '3'=>7.54)))
                ->if($resultat = $test->getVariance())
                ->then
                ->float($resultat)
                ->isEqualTo(6.7);
        }

        public function testEcartType() {
            $this
                ->given($test = new ObjectValue(array('0'=>14.2, '1'=>8.4, '2'=>11.0, '3'=>7.54)))
                ->if($resultat = $test->getEcartType())
                ->then
                ->float($resultat)
                ->isEqualTo(2.6);
        }

        public function testTauxDeReussite() {
            $this
                ->given($test = new ObjectValue(array('0'=>14.2, '1'=>8.4, '2'=>11.0, '3'=>7.54)))
                ->if($resultat = $test->getTaux())
                ->then
                ->float($resultat)
                ->isEqualTo(50.0);
        }
    }

}