<?php ob_start(); ?>
<?php use MarkAttack\librairie\Page;?>
    <style>
        .smiley {
            font-size:24px;
            color:green;
        }
        .hungry{
            font-size:24px;
            color:red;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="page-header">
                    <form>
                        <div class="form-group pull-right" >
                            <select id="report-object" class="form-control">
                                <?php foreach (Page::get('etudiant') as $key => $value): ?>
                                    <option <?php if ($index==$key) {echo 'selected="selected"';}?> value="<?php echo Trim($key);?>"><?php echo Trim($value);?></option>
                                <?php endforeach; ?>
                            </select>
                            <input type="hidden" id="report-param" value="index.php?url=reporting-student-">
                        </div>
                    </form>
                    <h2>Reporting par étudiant</h2>
                </div>
                <div class="row">
                    <table id="student-assess" class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center" colspan="5">Module</th>
                                <th class="text-center" colspan="2">Etudiant</th>
                            </tr>
                            <tr>
                                <th class="text-center col-sm-4">Nom</th>
                                <th class="text-center col-sm-1">Coefficient</th>
                                <th class="text-center col-sm-1">Moyenne</th>
                                <th class="text-center col-sm-1">Minimum</th>
                                <th class="text-center col-sm-1">Maximum</th>
                                <th class="text-center col-sm-2">Note</th>
                                <th class="text-center col-sm-2">Reussite</th>
                            </tr>
                        </thead>
                        <?php $moyenne=0.0; $taux=0.0; $variance=0.0; $nb=0; $defaut=0;?>
                        <tbody>
                        <?php foreach ($values as $key => $value): ?>
                            <tr>
                                <?php $nb = $nb + $value['coeff']; if($value['note']<10){$defaut = $defaut + $value['coeff'];}?>
                                <td class="text-left"><?php echo $value['name'];?></td>
                                <td class="text-center"><?php echo round($value['coeff'],1); ?></td>
                                <td class="text-center"><?php echo round($value['moyenne'],1); ?></td>
                                <td class="text-center"><?php echo round($value['mini'],1); ?></td>
                                <td class="text-center"><?php echo round($value['maxi'],1); ?></td>
                                <td class="text-center"><?php echo round($value['note'],1); ?></td>
                                <?php $moyenne = $moyenne + ($value['note'] * $value['coeff']);?>
                                <td class="text-center"><?php if($value['note']>=10):?><i class="glyphicon glyphicon-thumbs-up smiley"/><span class="hidden">1</span><?php else:?><i class="glyphicon glyphicon-thumbs-down hungry"/><span class="hidden">0</span><?php endif;?></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <?php foreach ($values as $key => $value): ?>
                                    <?php $variance = $variance + pow(($value['note']-($moyenne/$nb)),2);?>
                                <?php endforeach; if($nb){$variance = $variance/$nb;}?>
                                <th colspan="1" class="text-center">Moyenne: <?php echo ($nb)?round($moyenne/$nb,1):0.0;?></th>
                                <th colspan="4" class="text-center">Ecart Type: <?php echo ($nb)?round(sqrt($variance)):0.0;?></th>
                                <th colspan="2" class="text-center">Taux de Réussite: <?php echo ($nb)?round((($nb-$defaut)/$nb)*100):0;?>%</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>

<script>
    $(function () {
        $('#report-object').change( function() {
            location.href = $('#report-param').val() + $(this).val();
        });
    });
    $('#student-assess').DataTable();
</script>

<?php $content = ob_get_clean(); ?>
<?php require __DIR__ . '/../Layout.template.php'; ?>