<?php ob_start(); ?>
    <?php use MarkAttack\librairie\Page;?>
    <style>
        .smiley {
            font-size:24px;
            color:green;
        }
        .hungry{
            font-size:24px;
            color:red;
        }
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1"></div>
            <div class="col-sm-10">
                <div class="page-header">
                    <form>
                        <div class="form-group pull-right" >
                            <select id="report-object" class="form-control">
                                <?php foreach (Page::get('module') as $key => $value): ?>
                                    <option <?php if ($index==$key) {echo 'selected="selected"';}?> value="<?php echo Trim($key);?>"><?php echo Trim($value);?></option>
                                <?php endforeach; ?>
                            </select>
                            <input type="hidden" id="report-param" value="index.php?url=reporting-subject-">
                        </div>
                    </form>
                    <h2>Reporting par module</h2>
                </div>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">Répartition par classe</div>
                            <div class="panel-body">
                                <div id="chart-1"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-default">
                            <div class="panel-heading text-center">Répartition du taux Reussite/Défaut</div>
                            <div class="panel-body">
                                <div id="chart-2"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <table id="subject-report" class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">Etudiants</th>
                            <th class="text-center col-sm-2">Note</th>
                            <th class="text-center col-sm-2">Minimum</th>
                            <th class="text-center col-sm-2">Maximum</th>
                            <th class="text-center col-sm-2">Reussite</th>

                        </tr>

                        </thead>
                        <tbody>
                        <?php foreach ($values->getValues() as $key => $value) : ?>
                            <tr>
                                <td class="text-left"><?php echo $key;?></td>
                                <td class="text-center"><?php echo round($value,1);?></td>
                                <td class="text-center"><?php echo $values->getMin();?></td>
                                <td class="text-center"><?php echo $values->getMax();?></td>
                                <td class="text-center"><?php if($value>=10):?><i class="glyphicon glyphicon-thumbs-up smiley"/><span class="hidden">1</span><?php else:?><i class="glyphicon glyphicon-thumbs-down hungry"/><span class="hidden">0</span><?php endif;?></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th id="average" class="text-center">Moyenne: <?php echo $values->getMoyenne();?></th>
                                <th id="standard_deviation" colspan="2" class="text-center">Ecart Type: <?php echo $values->getEcartType();?></th>
                                <th id="success_threshold" colspan="2" class="text-center">Taux de Réussite: <?php echo $values->getTaux();?>%</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="col-sm-1"></div>
        </div>
    </div>
<?php $classes = $values->getClasse();?>
<script>
    $(function () {
        $('#report-object').change( function() {
            location.href = $('#report-param').val() + $(this).val();
        });
    });

    $('#subject-report').DataTable();

    var chart1 = c3.generate({
        bindto: '#chart-1',
        data: {
            columns: [
                ['data1',<?php echo $classes['[0 - 5['].','.$classes['[5 - 10['].','.$classes['[10 - 15['].','.$classes['[15 - 20]'];?>]
            ],
            type: 'bar',
            names: {
                data1: 'Effectifs'
            }
        },
        axis: {
            x: {
                type: 'category',
                categories: ['[0 - 5[', '[5 - 10[', '[10 - 15[', '[15 - 20]']
            }
        }
    });

    var chart2 = c3.generate({
        bindto: '#chart-2',
        data: {
            columns: [
                ['data1', <?php echo $values->getDefaut();?>],
                ['data2', <?php echo $values->getReussite();?>],
            ],
            type : 'donut',
            colors: {
                data1: '#ff0000',
                data2: '#00ff00'
            },
            names: {
                data1: 'Défaut',
                data2: 'Réussite'
            }
        },

        donut: {
            title: "Reussite/Défaut"
        }
    });



</script>

<?php $content = ob_get_clean(); ?>
<?php require __DIR__ . '/../Layout.template.php'; ?>