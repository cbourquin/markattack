<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 24/10/2018
 * Time: 11:18
 */

namespace MarkAttack\frontend\reporting;


class ObjectValue
{
    private $values;
    private $nb;
    private $min;
    private $max;

    /**
     * ObjectValue constructor.
     * @param $values
     */

    public function __construct($values)
    {
        if ($values or is_array($values)){
            $this->values = $values;
            $this->nb = count($values);
            $this->min = ($this->nb>0)?round(min(array_values($values)),1):0.0;
            $this->max = ($this->nb>0)?round(max(array_values($values)),1):0.0;
        } else {
            $this->values = $values;
            $this->nb = 0;
            $this->min = 0.0;
            $this->max = 0.0;
        }
    }

    /**
     * @return mixed
     */
    public function getValues()
    {
        return $this->values;
    }

    public function getMoyenne(){
        $sum = 0.0;
        foreach ($this->values as $key=>$value)
            $sum = $sum + $value;
        return ($this->nb!=0)?round($sum/$this->nb,1):0.0;
    }

    public function getDefaut(){
        $defaut = 0;
        foreach ($this->values as $key=>$value){
            if ($value<10)
                $defaut++;
        }
        return $defaut;
    }

    public function getReussite(){
        return ($this->nb!=0)?($this->nb - self::getDefaut()):0;
    }

    public function getTaux(){
        $defaut = self::getDefaut();
        return ($this->nb!=0)?round((($this->nb-$defaut) * 100) / $this->nb,1):0.0;
    }

    public function getVariance(){
        $variance = 0.0;
        $moyenne = self::getMoyenne();
        foreach ($this->values as $key => $value)
            $variance = $variance + pow(($moyenne-$value),2);
        if ($this->nb!=0)
            $variance = $variance/$this->nb;
        return round($variance,1);
    }

    public function getEcartType(){
        return ($this->nb!=0)?round(sqrt($this->getVariance()),1):0.0;
    }

    public function getMin(){
        return $this->min;
    }

    public function getMax(){
        return $this->max;
    }

    public function getClasse(){
        $classe1=0; $classe2=0; $classe3=0; $classe4=0;
        foreach ($this->values as $key => $value){
            if(($value>=0) and ($value<5)){$classe1++;}
            if(($value>=5) and ($value<10)){$classe2++;}
            if(($value>=10) and ($value<15)){$classe3++;}
            if($value>=15){$classe4++;}
        }
        $res['[0 - 5['] = $classe1;
        $res['[5 - 10['] = $classe2;
        $res['[10 - 15['] = $classe3;
        $res['[15 - 20]'] = $classe4;
        return $res;
    }

}