<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 24/09/2018
 * Time: 23:24
 */

namespace MarkAttack\frontend\reporting;

use MarkAttack\entity\Person;
use MarkAttack\librairie\Page;
use MarkAttack\frontend\model\ModelReport;
use MarkAttack\frontend\model\ModelSettings;
use MarkAttack\frontend\reporting\ObjectValue;

require_once dirname(__FILE__).'/../model/ModelReport.php';
require_once dirname(__FILE__).'/ObjectValue.php';

class ActionReport{

    private static function getModules($id){
        $user =  unserialize($_SESSION['user']);
        $result = ModelSettings::getModules(($user->FK_Person_Type != Person::ROLE_ADMIN)?$user->id:null);
        Page::set('module', $result);

        if(!in_array($id, array_keys($result)))
            return current(array_keys($result));
        return $id;
    }

    private static function getStudents($id=null){
        $user =  unserialize($_SESSION['user']);
        $result = ModelReport::getStudents(($user->FK_Person_Type != Person::ROLE_ADMIN)?$user->id:null);
        Page::set('etudiant', $result);

        if(!in_array($id, array_keys($result)))
            return current(array_keys($result));
        return $id;
    }

    public static function reportSubject($subject){
        session_start();
        if (isset($_SESSION['user'])){
            switch (unserialize($_SESSION['user'])->FK_Person_Type){
                case Person::ROLE_ADMIN:
                    $index = self::getModules($subject);
                    $values = new ObjectValue(ModelReport::getSubjectReport($index));
                    require_once 'subjects.template.php';
                    break;

                case Person::ROLE_ENSEIGNANT:
                    $index = self::getModules($subject);
                    $values = new ObjectValue(ModelReport::getSubjectReport($index));
                    require_once 'subjects.template.php';
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function reportStudent($student){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $index = self::getStudents($student);
                    $values = ModelReport::getStudentReport($index);
                    require_once 'students.template.php';
                    break;

                case Person::ROLE_ENSEIGNANT:
                    $index = self::getStudents($student);
                    $values = ModelReport::getStudentReport($index);
                    require_once 'students.template.php';
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }

        } else {
            Page::display('index.php?url=index');
        }
    }

}