<?php use MarkAttack\entity\Person; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Mark Attack</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="resources/css/bootstrap.min.css">
        <link rel="stylesheet" href="resources/css/font-awesome.min.css">
		<link href="resources/css/montserrat.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="resources/css/dataTables.bootstrap.min.css">

        <script type="text/javascript" language="javascript" src="resources/js/jquery-3.3.1.js"></script>
        <script type="text/javascript" language="javascript" src="resources/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="resources/js/dataTables.bootstrap.min.js"></script>
		<script src="resources/js/bootstrap.min.js"></script>

        <link href="resources/c3-d3/c3.css" rel="stylesheet">
        <script src="resources/c3-d3/docs/js/d3-5.4.0.min.js" charset="utf-8"></script>
        <script src="resources/c3-d3/c3.min.js"></script>

    </head>
    <body>
    <?php if (isset($_SESSION['user'])): ?>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><strong>Mark Attack</strong></a>
                </div>

                <ul class="nav navbar-nav">
                    <li><a href="index.php?url=home">Accueil</a></li>
                    <?php if (in_array(unserialize($_SESSION['user'])->FK_Person_Type, array(Person::ROLE_ENSEIGNANT,Person::ROLE_ADMIN))):?>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Reporting
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="index.php?url=reporting-subject-{id}">Module</a></li>
                            <li><a href="index.php?url=reporting-student-{id}">Etudiant</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            Configuration
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header"><strong>Gestion d'un module</strong></li>
                            <li ><a href="index.php?url=settings-teacher-subject-{id}">Enseignant</a></li>
                            <li ><a href="index.php?url=settings-student-subject-{id}">Etudiant</a></li>
                            <li><a href="index.php?url=initGrades">Notation</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-header"><strong>Administrateur</strong></li>
                            <?php if (unserialize($_SESSION['user'])->FK_Person_Type==Person::ROLE_ADMIN):?>
                                <li><a href="index.php?url=settings-user-{user}">Utilisateur</a></li>
                            <?php endif;?>
                                <li ><a href="index.php?url=settings-subject-{subject}">Module</a></li>

                        </ul>
                    </li>
                    <?php endif;?>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li><a id="ConnectedUserName"><i class="glyphicon glyphicon-user"></i> <?php echo Trim(unserialize($_SESSION['user'])->Person_Firstname) . " ". Trim(unserialize($_SESSION['user'])->Person_Lastname);?></a></li>
                    <li><a id="disconect" href="index.php?url=log-out"><i class="glyphicon glyphicon-log-out"></i> Deconnexion</a></li>
                </ul>
            </div>
        </nav>
    <?php else: ?>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><strong>Mark Attack</strong></a>
                </div>
            </div>
        </nav>
    <?php endif;?>
    <body>
		<?= $content ?>
    </body>
</html>
