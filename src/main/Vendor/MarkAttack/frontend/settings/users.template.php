<?php ob_start(); ?>
<div class="container-fluid">
	<div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="row">
                <div class="page-header">
                    <a id="user-icon" class="btn btn-primary pull-right" data-toggle="collapse" data-parent="#accordion" href="#users">
                        <i class="glyphicon glyphicon-plus"></i>
                    </a>
                    <h2>Gestion des utilisateurs</h2>
                </div>

                <div id="users" class="panel-collapse collapse <?php echo ($ind or (isset($_SESSION['errors'])))?'in':'';?>">
                    <h4 style="padding-left: 20px;"><strong><?php echo ($ind)?'Modifier':'Ajouter';?> un utilisateur</strong></h4>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="container-fluid">
                            <div class="row">
                                <form action="index.php?url=settings-<?php echo ($ind)?'update':'add';?>-user<?php echo ($ind)?'-'.$_POST['id']:'';?>" method="POST">
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                            <label for="lastname">Nom :</label>
                                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="" value="<?php echo ($ind)?$_POST['lastname']:'';?>">
                                            <div class="invalid-feedback"><?php echo ((isset($_SESSION['errors']['Person_Lastname']))?$_SESSION['errors']['Person_Lastname']:'');?></div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="firstname">Prénom :</label>
                                            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="" value="<?php echo ($ind)?$_POST['firstname']:'';?>">
                                            <div class="invalid-feedback"><?php echo ((isset($_SESSION['errors']['Person_Firstname']))?$_SESSION['errors']['Person_Firstname']:'');?></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                            <label for="pseudo">Identifiant :</label>
                                            <input type="text" class="form-control" id="pseudo" name="login" placeholder="" value="<?php echo ($ind)?$_POST['login']:'';?>">
                                            <div class="invalid-feedback"><?php echo ((isset($_SESSION['errors']['Person_Login']))?$_SESSION['errors']['Person_Login']:'');?></div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="pwd">Mot de passe :</label>
                                            <input type="password" class="form-control" id="pwd" name="pwd" placeholder="" <?php echo ($ind)?'disabled':'';?>>
                                            <div class="invalid-feedback"><?php echo ((isset($_SESSION['errors']['Person_Password']))?$_SESSION['errors']['Person_Password']:'');?></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                            <label for="email">Email :</label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com" value="<?php echo ($ind)?$_POST['email']:'';?>">
                                            <div class="invalid-feedback"><?php echo ((isset($_SESSION['errors']['Person_Mail']))?$_SESSION['errors']['Person_Mail']:'');?></div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="tPerson">Statut :</label>
                                            <select class="form-control" name="tPerson" id="tPerson">
                                                <option <?php if ($ind) {echo ($_POST['tPerson']==1)?'selected="selected"':'';}?> value="1">Etudiant</option>
                                                <option <?php if ($ind) {echo ($_POST['tPerson']==2)?'selected="selected"':'';}?> value="2">Enseignant</option>
                                                <option <?php if ($ind) {echo ($_POST['tPerson']==3)?'selected="selected"':'';}?> value="3">Administrateur</option>
                                            </select>
                                            <div class="invalid-feedback"><?php echo ((isset($_SESSION['errors']['FK_Person_Type']))?$_SESSION['errors']['FK_Person_Type']:'');?></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <button class="btn btn-primary pull-right"><?php echo ($ind)?'Modifier':'Ajouter';?> un utilisateur</button>
                                    </div>
                                    <br>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>

                <table id="users-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">Nom</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Pseudonyme</th>
                        <th class="text-center">Statut</th>
                        <th class="text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php $role=array(1=>'Etudiant', 2=>'Enseignant', 3=>'Administrateur');?>
                        <?php foreach ($persons as $person):?>
                        <tr>
                            <td class="text-left"><?php echo $person->Person_Lastname . ' ' . $person->Person_Firstname;?></td>
                            <td class="text-left"><?php echo $person->Person_Mail;?></td>
                            <td class="text-left"><?php echo $person->Person_Login;?></td>
                            <td class="text-left"><?php echo $role[$person->FK_Person_Type];?></td>
                            <td class="text-right">
                                <a href="index.php?url=settings-user-<?php echo $person->id;?>" id="edit_<?php echo $person->id;?>" type="button" class="btn btn-default">Modif. <i class="glyphicon glyphicon-pencil"></i></a>
                                <a href="index.php?url=settings-delete-user-<?php echo $person->id;?>" id="delete_<?php echo $person->id;?>" type="button" class="btn btn-default">Suppr. <i class="glyphicon glyphicon-trash"></i></a>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-1"></div>
	</div>
</div>
<script>
    $('#users-table').DataTable();
</script>
<?php $content = ob_get_clean(); ?>
<?php require_once dirname(__FILE__) . '/../Layout.template.php'; ?>