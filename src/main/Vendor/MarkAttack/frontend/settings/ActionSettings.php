<?php

namespace MarkAttack\frontend\settings{


    use MarkAttack\error\ValidationException;
    use MarkAttack\entity\Person;
    use MarkAttack\entity\Module;
    use MarkAttack\frontend\model\ModelSettings;
    use MarkAttack\librairie\Page;

    class ActionSettings
{

    /*
     * Configuration de la gestion des utilisateurs
     * */

    public static function gestionUtilisateur($user){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $ind = false;
                    $persons = ModelSettings::getPersons(unserialize($_SESSION['user'])->id);
                    if (in_array($user, array_keys(Page::get('Persons')))) {
                        $_POST['firstname'] = $persons[$user]->Person_Firstname;
                        $_POST['lastname'] = $persons[$user]->Person_Lastname;
                        $_POST['login'] = $persons[$user]->Person_Login;
                        $_POST['email'] = $persons[$user]->Person_Mail;
                        $_POST['tPerson'] = $persons[$user]->FK_Person_Type;
                        $_POST['id'] = $persons[$user]->id;
                        $ind = true;
                    }
                    require_once 'users.template.php';
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function ajouterUtilisateur(){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $firstname = $_POST['firstname'];
                    $lastname = $_POST['lastname'];
                    $login = $_POST['login'];
                    $pass = $_POST['pwd'];
                    $email = $_POST['email'];
                    $tPerson = $_POST['tPerson'];

                    $p = new Person(null,$lastname,$firstname,$tPerson,$email,$login,$pass);
                    try {
                        $p->save();
                    } catch (ValidationException $e){
                        $_SESSION['errors'] = $e->getErrors();
                        Page::displayErrs('index.php?url=settings-user-{user}');
                    }
                    Page::display('index.php?url=settings-user-{user}');
                    break;
                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function modifierUtilisateur($user){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    ModelSettings::getPersons(unserialize($_SESSION['user'])->id);
                    if (in_array($user, array_keys(Page::get('Persons')))){
                        $p = new Person($user);
                        $p->Person_Firstname = $_POST['firstname'];
                        $p->Person_Lastname = $_POST['lastname'];
                        $p->Person_Login = $_POST['login'];
                        $p->Person_Mail = $_POST['email'];
                        $p->FK_Person_Type = $_POST['tPerson'];

                        try {
                            $p->save();
                        } catch (ValidationException $e){
                            $_SESSION['errors'] = $e->getErrors();
                            Page::displayErrs('index.php?url=settings-user-'.$user);
                        }
                    }
                    Page::display('index.php?url=settings-user-{user}');
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function supprimerUtilisateur($user){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $p = new Person($user);
                    $p->remove();
                    Page::display('index.php?url=settings-user-{user}');
                    break;
                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }






    public static function gestionModule($subject){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $ind = false;
                    $subjects = ModelSettings::getModules2();
                    if (in_array($subject, Page::get('Subjects'))){
                        $_POST['id'] = $subjects[$subject]['class']->id;
                        $_POST['subject'] = $subjects[$subject]['class']->Module_Name;
                        $_POST['coefficient'] = $subjects[$subject]['class']->Module_Coefficient;
                        $ind = true;
                    }
                    require_once 'subjects.template.php';
                    break;

                case Person::ROLE_ENSEIGNANT:
                    $subjects = ModelSettings::getModules2(unserialize($_SESSION['user'])->id);
                    $ind = false;
                    if (in_array($subject, Page::get('Subjects'))){
                        $_POST['id'] = $subjects[$subject]['class']->id;
                        $_POST['subject'] = $subjects[$subject]['class']->Module_Name;
                        $_POST['coefficient'] = $subjects[$subject]['class']->Module_Coefficient;
                        $ind = true;
                    }
                    require_once 'subjects.template.php';
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function ajouterModule(){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $sub = $_POST['subject'];
                    $coef = $_POST['coefficient'];

                    $m = new Module(null, $sub, $coef);
                    try {
                        $m->save();
                    } catch (ValidationException $exception){
                        $_SESSION['errors'] = $exception->getErrors();
                        Page::displayErrs('index.php?url=settings-subject-{subject}');
                    }
                    Page::display('index.php?url=settings-subject-{subject}');
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function modifierModule($subject){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $subjects = ModelSettings::getModules2();
                    if (in_array($subject, Page::get('Subjects'))){
                        $m = new Module($subject);
                        $m->Module_Name = $_POST['subject'];
                        $m->Module_Coefficient = $_POST['coefficient'];
                        try{
                            $m->save();
                        }catch (ValidationException $exception){
                            $_SESSION['errors'] = $exception->getErrors();
                            Page::displayErrs('index.php?url=settings-subject-{subject}');
                        }
                    }
                    Page::display('index.php?url=settings-subject-{subject}');
                    break;

                case Person::ROLE_ENSEIGNANT:
                    $subjects = ModelSettings::getModules2(unserialize($_SESSION['user'])->id);
                    if (in_array($subject, Page::get('Subjects'))){
                        $m = new Module($subject);
                        $m->Module_Coefficient = $_POST['coefficient'];
                        try{
                            $m->save();
                        }catch (ValidationException $exception){
                            $_SESSION['errors'] = $exception->getErrors();
                            Page::displayErrs('index.php?url=settings-subject-{subject}');
                        }
                    }
                    Page::display('index.php?url=settings-subject-{subject}');
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function supprimerModule($subject){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $subjects = ModelSettings::getModules2();
                    if (in_array($subject, Page::get('Subjects'))){
                        $m = new Module($subject);
                        $m->remove();
                    }
                    Page::display('index.php?url=settings-subject-{subject}');
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }
}
}

?>