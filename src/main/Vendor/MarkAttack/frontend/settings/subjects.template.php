<?php ob_start(); ?>
<?php use MarkAttack\entity\Person;?>
<div class="container-fluid">
	<div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="row">
                <div class="page-header">
                    <?php if (unserialize($_SESSION['user'])->FK_Person_Type == Person::ROLE_ADMIN):?>
                        <a id="user-icon" class="btn btn-primary pull-right" data-toggle="collapse" data-parent="#accordion" href="#subjects">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    <?php endif;?>
                    <h2>Gestion des modules</h2>
                </div>

                <?php if ((unserialize($_SESSION['user'])->FK_Person_Type==Person::ROLE_ADMIN) or ((unserialize($_SESSION['user'])->FK_Person_Type==Person::ROLE_ENSEIGNANT) and ($ind))):?>
                <div id="subjects" class="panel-collapse collapse <?php echo ($ind or isset($_SESSION['errors']))?'in':'';?>">
                    <h4 style="padding-left: 20px;"><strong><?php echo ($ind)?'Modifier':'Ajouter';?> un module</strong></h4>
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="container-fluid">
                            <div class="row">
                                <form action="index.php?url=settings-<?php echo ($ind)?'update':'add';?>-subject<?php echo ($ind)?'-'.$_POST['id']:'';?>" method="POST">
                                    <div class="row">
                                        <div class="col-md-6 mb-3">
                                            <label for="subject">Module :</label>
                                            <input type="text" class="form-control" id="subject" name="subject" placeholder="" value="<?php echo ($ind)?$_POST['subject']:'';?>" <?php echo (unserialize($_SESSION['user'])->FK_Person_Type==Person::ROLE_ENSEIGNANT)?'disabled':'';?>>
                                            <div class="invalid-feedback"><?php echo ((isset($_SESSION['errors']['Module_Name']))?$_SESSION['errors']['Module_Name']:'');?></div>
                                        </div>
                                        <div class="col-md-6 mb-3">
                                            <label for="coefficient">Coefficient :</label>
                                            <input type="text" class="form-control" id="coefficient" name="coefficient" placeholder="" value="<?php echo ($ind)?$_POST['coefficient']:'';?>">
                                            <div class="invalid-feedback"><?php echo ((isset($_SESSION['errors']['Module_Coefficient']))?$_SESSION['errors']['Module_Coefficient']:'');?></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <button class="btn btn-primary pull-right"><?php echo ($ind)?'Modifier':'Ajouter';?> un module</button>
                                    </div>
                                    <br>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
                <?php endif;?>

                <table id="subjects-table" class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center">Module</th>
                        <th class="text-center col-sm-2">Coefficient</th>
                        <th class="text-center">Enseignant(s)</th>
                        <th class="text-center col-sm-3">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($subjects as $subject):?>
                        <tr>
                            <td class="text-left"><?php echo $subject['class']->Module_Name;?></td>
                            <td class="text-center"><?php echo $subject['class']->Module_Coefficient;?></td>
                            <td class="text-left"><?php echo $subject['teachers'];?></td>
                            <td class="text-right">
                                <?php if(unserialize($_SESSION['user'])->FK_Person_Type==Person::ROLE_ADMIN):?>
                                    <a href="index.php?url=settings-subject-<?php echo $subject['class']->id;?>" id="edit_<?php echo $subject['class']->id;?>" type="button" class="btn btn-default">Modif. <i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="index.php?url=settings-delete-subject-<?php echo $subject['class']->id;?>" id="delete_<?php echo $subject['class']->id;?>" type="button" class="btn btn-default">Suppr. <i class="glyphicon glyphicon-trash"></i></a>
                                <?php else :?>
                                    <a href="index.php?url=settings-subject-<?php echo $subject['class']->id;?>" type="button" class="btn btn-default">Modif. <i class="glyphicon glyphicon-pencil"></i></a>
                                <?php endif;?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-1"></div>
	</div>
</div>
<script>
    $('#subjects-table').DataTable();
</script>
<?php $content = ob_get_clean(); ?>
<?php require_once dirname(__FILE__) . '/../Layout.template.php'; ?>