<?php ob_start(); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
               <div class="page-header">
                   <h3><?php echo $title;?></h3>
               </div>
                <p><?php echo $explain;?></p>
                <ul>
                    <li>Ouziel KELEKELE: Ouziel.kelekele@edu.univ-fcomte.fr</li>
                    <li>Guzal Khusanova: Guzal.Khusanova@edu.univ-fcomte.fr</li>
                    <li>Tony LOMBARDO: Tony.LOMBARDO@edu.univ-fcomte.fr</li>
                    <li>Arthur DEBOST: Arthur.DEBOST@edu.univ-fcomte.fr</li>
                    <li>Clement BOURQUIN : Clement.BOURQUIN@edu.univ-fcomte.fr</li>
                    <li>Julien VANNIER: Julien.VANNIER03@edu.univ-fcomte.fr</li>
                </ul>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
<?php $content = ob_get_clean(); ?>
<?php require_once dirname(__FILE__) . "/../Layout.template.php"; ?>