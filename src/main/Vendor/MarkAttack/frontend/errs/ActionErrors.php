<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 24/09/2018
 * Time: 23:24
 */

class ActionErrors
{
    public static function errs404(){
        session_start();
        $title = 'Erreur 404: Page non trouvée';
        $explain = "La page demandée n'existe pas.</br>
                    Si le problème persiste, veuillez contactez le SAV: ";
        require_once 'error.template.php';
    }

    public static function errsDenied(){
        session_start();
        $title = 'Erreur 400: Accés non autorisée';
        $explain = "<strong>Vous ne disposez pas des droits</strong> pour accéder a cette page.</br>
                    Pour accédez a cette page, veuillez faire une demande auprès du SAV: ";
        require_once 'error.template.php';
    }

    public static function errsMaintenance(){
        $title = 'Erreur 402: Site est en cours de maintenance ';
        $explain = "Le site est en cours de maintenace.</br>
                    Veuillez contacter le SAV pour plus d'informations: ";
        require_once 'error.template.php';
    }

}