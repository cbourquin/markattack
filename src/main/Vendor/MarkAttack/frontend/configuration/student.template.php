<?php use MarkAttack\librairie\Page;?>
<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong>Liste des étudiants incrit au module</strong>
        </div>
        <div class="panel-body">
            <table id="student-in" class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">Nom</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Moyenne</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($list as $key => $value) :?>
                    <tr>
                        <td id="registered_student_<?php echo $value->id;?>"><?php echo $value->Person_Lastname . ' ' . $value->Person_Firstname;?></td>
                        <td><?php echo $value->Person_Mail; ?></td>
                        <td class="text-center"><?php $note = $value->getGradeInModule($id);if($note){echo $note->Grade_Value;}?></td>
                        <td class="text-right">
                            <a href="index.php?url=settings-delete-student-<?php echo trim($value->id);?>-subject-<?php echo trim($id);?>" type="button" class="btn btn-default">Suppr. <i class="glyphicon glyphicon-trash"></i></a>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="panel panel-default">
        <div class="panel-heading"><strong> Liste des étudiants non-incrit au module</strong></div>
        <div class="panel-body">
            <table id="student-out" class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">Nom</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($notList as $p) :?>
                    <tr>
                        <td id="unregistered_student_<?php echo $p->id;?>"><?php echo $p->Person_Lastname . ' ' . $p->Person_Firstname;?></td>
                        <td><?php echo $p->Person_Mail; ?></td>
                        <td class="text-right">
                            <?php if (Page::get('modules')):?>
                            <a href="index.php?url=settings-add-student-<?php echo trim($p->id);?>-subject-<?php echo trim($id);?>" id="add_<?php echo trim($p->id);?>" type="button" class="btn btn-default">Ajouter <i class="glyphicon glyphicon-plus"></i></a>
                            <?php endif;?>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('#settings-object').change( function() {
            location.href = $('#settings-param').val() + $(this).val();
        });
    });

    $('#student-out').DataTable();
    $('#student-in').DataTable();
</script>