<?php ob_start(); ?>
<div class="container-fluid">
	<div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="row">
                <div class="page-header">
                    <h2>Information</h2>
                </div>
                Aucun module n'est affecté à votre nom.<br>
                Merci de contactez un administrateur pour vous affectez a un module, afin de benéficier de ce service.
            </div>
        </div>
        <div class="col-sm-1"></div>
	</div>
</div>
<?php $content = ob_get_clean(); ?>
<?php require_once dirname(__FILE__) . '/../Layout.template.php'; ?>