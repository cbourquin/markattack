<?php ob_start(); ?>
<?php use MarkAttack\librairie\Page;?>
<div class="container-fluid">
	<div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="row">
                <div class="page-header">
                    <?php if (Page::get('modules')):?>
                    <form>
                        <div class="form-group pull-right" >
                            <select id="settings-object" class="form-control">
                                <?php foreach (Page::get('modules') as $key => $value): ?>
                                    <option <?php if ($id==$key) {echo 'selected="selected"';}?> value="<?php echo Trim($key);?>"><?php echo Trim($value);?></option>
                                <?php endforeach; ?>
                            </select>
                            <input type="hidden" id="settings-param" value="<?php echo 'index.php?url=settings-' . trim($action) . '-subject-';?>">
                        </div>
                    </form>
                    <?php endif;?>
                    <h2>Gestion d'un module</h2>
                </div>
                <?php
                    switch ($action){
                        case 'teacher':
                            require_once 'teacher.template.php';
                            break;
                        case 'student':
                            require_once 'student.template.php';
                            break;
                    }
                ?>
            </div>
        </div>
        <div class="col-sm-1"></div>
	</div>
</div>
<?php $content = ob_get_clean(); ?>
<?php require_once dirname(__FILE__) . '/../Layout.template.php'; ?>