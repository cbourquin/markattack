<?php use \MarkAttack\entity\Person; ?>
<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong>Liste des professeurs inscrit au module</strong>
        </div>
        <div class="panel-body">
            <table id="teacher-in" class="table table-bordered">
                <thead>
                <tr>
                    <th class="text-center">Nom</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($teachers as $p) :?>
                    <tr>
                        <td id="registered_teacher_<?php echo $p->id;?>"><?php echo $p->Person_Lastname . ' ' . $p->Person_Firstname;?></td>
                        <td><?php echo $p->Person_Mail; ?></td>
                        <td class="text-right">
                            <?php if((unserialize($_SESSION['user'])->FK_Person_Type==Person::ROLE_ADMIN) or ((unserialize($_SESSION['user'])->FK_Person_Type==Person::ROLE_ENSEIGNANT) and (unserialize($_SESSION['user'])->id==$p->id))):?>
                            <a href="index.php?url=settings-remove-teacher-<?php echo trim($p->id);?>-subject-<?php echo trim($id);?>" id="quit_<?php echo trim($p->id);?>" type="button" class="btn btn-default">
                                Quit. <i class="glyphicon glyphicon-trash"></i>
                            </a>
                            <?php endif;?>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <div class="panel panel-default">
        <div class="panel-heading">
            <strong>Liste des professeurs non-inscrit au module</strong>
        </div>
        <div class="panel-body">
            <table id="teacher-out" class="table table-bordered">
                <thead>
                    <tr>
                        <th class="text-center">Nom</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($peoples as $p) :?>
                        <tr>
                            <td id="unregistered_teacher_<?php echo $p->id;?>"><?php echo $p->Person_Lastname . ' ' . $p->Person_Firstname;?></td>
                            <td><?php echo $p->Person_Mail; ?></td>
                            <td class="text-right">
                                <a href="index.php?url=settings-add-teacher-<?php echo trim($p->id);?>-subject-<?php echo trim($id);?>" id="add_<?php echo trim($p->id);?>" type="button" class="btn btn-default">Ajouter <i class="glyphicon glyphicon-plus"></i></a>

                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function () {
        $('#settings-object').change( function() {
            location.href = $('#settings-param').val() + $(this).val();
        });
    });

    $('#teacher-in').DataTable();
    $('#teacher-out').DataTable();

</script>