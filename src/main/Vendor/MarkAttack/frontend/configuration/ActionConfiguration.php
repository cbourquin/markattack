<?php

namespace MarkAttack\frontend\configuration;

use MarkAttack\entity\Person;
use MarkAttack\entity\Module;
use MarkAttack\librairie\Page;
use MarkAttack\frontend\model\ModelSettings;

class ActionConfiguration
{

    private static function getModules($id){
        $user =  unserialize($_SESSION['user']);
        $result = ModelSettings::getModules(($user->FK_Person_Type != Person::ROLE_ADMIN)?$user->id:null);
        Page::set('modules', $result);

        if(!in_array($id, array_keys($result)))
            return current(array_keys($result));
        return $id;
    }


    private static function getStudents($module, $student, $not=true){
        if ($not)
            $result = ModelSettings::getStudentNotRegisterToModule($module);
        else
            $result = ModelSettings::getStudentRegisterToModule($module);

        if(!in_array($student, array_keys($result)))
            return false;
        return $student;
    }


    private static function getProfesseur($id){
        $result = ModelSettings::getProfesseur();
        if(!in_array($id, $result))
            return false;
        return $id;
    }

    /*
     *  Configuration de la gestion d'utilisateur dans le module
     * */

    public static function configModules($id){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $id = self::getModules($id);
                    $m = new Module($id);
                    $list = $m->getStudent();
                    $notList = $m->getNotStudent();
                    $action = 'student';
                    require_once 'main.template.php';
                    break;

                case Person::ROLE_ENSEIGNANT:
                    $id = self::getModules($id);
                    if ($id!=false) {
                        $m = new Module($id);
                        $list = $m->getStudent();
                        $notList = $m->getNotStudent();
                        $action = 'student';
                        require_once 'main.template.php';
                    } else {
                        require_once 'ask.template.php';
                    }
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function ajoutModules($student, $subject){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $subject = self::getModules($subject);
                    if ($subject!=false){
                        $student = self::getStudents($subject, $student);
                        if ($student) {
                            $m = new Module($subject);
                            $m->addPerson($student);
                        }
                        Page::display('index.php?url=settings-student-subject-' . $subject);
                    } else {
                        Page::display('index.php?url=settings-student-subject-{id}');
                    }
                    break;

                case Person::ROLE_ENSEIGNANT:
                    $subject = self::getModules($subject);
                    if ($subject!=false) {
                        $student = self::getStudents($subject, $student);
                        if ($student!=false) {
                            $m = new Module($subject);
                            $m->addPerson($student);
                        }
                        Page::display('index.php?url=settings-student-subject-'.$subject);
                    }else{
                        Page::display('index.php?url=settings-student-subject-{id}');
                    }
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function supprimeModules($student, $subject){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $subject = self::getModules($subject);
                    if ($subject!=false) {
                        $student = self::getStudents($subject, $student, false);
                        if ($student!=false) {
                            $m = new Module($subject);
                            $m->delPerson($student);
                        }
                        Page::display('index.php?url=settings-student-subject-' . $subject);
                    } else {
                        Page::display('index.php?url=settings-student-subject-{id}');
                    }
                    break;

                case Person::ROLE_ENSEIGNANT:
                    $subject = self::getModules($subject);
                    if ($subject!=false) {
                        $student = self::getStudents($subject, $student, false);
                        if ($student!=false) {
                            $m = new Module($subject);
                            $m->delPerson($student);
                        }
                        Page::display('index.php?url=settings-student-subject-' . $subject);
                    } else {
                        Page::display('index.php?url=settings-student-subject-{id}');
                    }
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }




    /*
     *  Configuration de l'ajout de professeur dans le module
     * */

    public static function ajoutProfModule($id){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $id = self::getModules($id);
                    $m = new Module($id);
                    $peoples = $m->getTeacherNotList();
                    $teachers = $m->getTeacher();
                    $action = 'teacher';
                    require_once 'main.template.php';
                    break;

                case Person::ROLE_ENSEIGNANT:
                    $id = self::getModules($id);
                    if ($id!=false) {
                        $m = new Module($id);
                        $peoples = $m->getTeacherNotList();
                        $teachers = $m->getTeacher();
                        $action = 'teacher';
                        require_once 'main.template.php';
                    }else {
                        require_once 'ask.template.php';
                    }
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function ajoutProf($teacher, $subject){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $subject = self::getModules($subject);
                    $teacher = self::getProfesseur($teacher);
                    if($teacher and $subject){
                       $p = new Person($teacher);
                       $p->addToModule($subject);
                    }
                    Page::display('index.php?url=settings-teacher-subject-'.$subject);
                    break;

                case Person::ROLE_ENSEIGNANT:
                    $subject = self::getModules($subject);
                    $teacher = self::getProfesseur($teacher);

                    if($teacher and $subject){
                        $p = new Person($teacher);
                        $p->addToModule($subject);
                    }
                    Page::display('index.php?url=settings-teacher-subject-'.$subject);
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

    public static function deleteProf($teacher, $subject){
        session_start();
        if (isset($_SESSION['user'])) {
            switch (unserialize($_SESSION['user'])->FK_Person_Type) {
                case Person::ROLE_ADMIN:
                    $subject = self::getModules($subject);
                    $teacher = self::getProfesseur($teacher);
                    if($teacher and $subject){
                       $p = new Person($teacher);
                       $p->removeToModule($subject);
                    }
                    Page::display('index.php?url=settings-teacher-subject-{id}');
                    break;

                case Person::ROLE_ENSEIGNANT:
                    $subject = self::getModules($subject);
                    $teach = ($teacher==unserialize($_SESSION['user'])->id);
                    if($teach and $subject){
                        $p = new Person($teacher);
                        $p->removeToModule($subject);
                    }
                    Page::display('index.php?url=settings-teacher-subject-{id}');
                    break;

                default :
                    Page::display('index.php?url=error-denied');
                    break;
            }
        } else {
            Page::display('index.php?url=index');
        }
    }

}

?>