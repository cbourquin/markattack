<?php

namespace MarkAttack\frontend\grades {

    use MarkAttack\entity\Grade;
    use MarkAttack\entity\ObjectFactory;
    use MarkAttack\entity\Person;
    use MarkAttack\error\ValidationException;

    require_once dirname(__FILE__) . '/../../entity/Person.php';
    require_once dirname(__FILE__) . '/../../error/ValidationException.php';
    require_once dirname(__FILE__) . '/../../entity/Grade.php';

    class GradeLine
    {
        private $grade;
        private $person;
        private $objectFactory;
        const NEW_GRADE_STYLE = "warning";
        const EXISTING_GRADE_STYLE = "success";
        const ERROR_GRADE_STYLE = "danger";

        private function __construct(Person $person, Grade $grade, ObjectFactory $objectFactory)
        {
            $this->person = $person;
            $this->grade = $grade;
            $this->objectFactory = $objectFactory;
        }

        /*
         * Renderings
         */

        public function gradeValue()
        {
            return $this->grade->getGradeValue();
        }

        public function uniqueName()
        {
            return "gradeLine" . $this->person->id;
        }

        public function lastName()
        {
            return $this->person->getPersonLastname();
        }

        public function firstName()
        {
            return $this->person->getPersonFirstname();
        }


        public function styleClassName()
        {
            $gradeValue = $this->grade->getGradeValue();
            if ($gradeValue == null) {
                return $this::NEW_GRADE_STYLE;
            }
            if ($this->isCorrectGradeValue($gradeValue)) {
                return $this::EXISTING_GRADE_STYLE;
            }
            return $this::ERROR_GRADE_STYLE;
        }

        public function isRequired()
        {
            if ($this->grade->getId() != null) {
                return "required";
            }
            return "";
        }

        /*
         * Control
         */
        public function save($gradeValue)
        {
            if ($this->getGrade()->getGradeValue() != $gradeValue) {
                try {
                    $this->grade->setGradeValue($gradeValue);
                    $this->grade->save();
                } catch (ValidationException $e) {
                }
            }
        }
        private function isCorrectGradeValue($gradeValue)
        {
            return is_numeric($gradeValue) && $gradeValue >= 0 && $gradeValue <= 20;
        }

        public function hasWrongGradeValue()
        {
            return $this->gradeValue() != null && !$this->isCorrectGradeValue($this->grade->getGradeValue());
        }


        public function getPerson()
        {
            return $this->person;
        }

        public function getGrade()
        {
            return $this->grade;
        }




        /**
         *  CREATE
         */

        public static function createExistingGradeLine($gradeID, ObjectFactory $objectFactory)
        {
            $grade = $objectFactory->createGrade($gradeID);
            return new GradeLine($objectFactory->createPerson($grade->FK_Student), $grade, $objectFactory);
        }

        public static function createNewGradeLine($personId, $moduleId, ObjectFactory $objectFactory)
        {
            return new GradeLine($objectFactory->createPerson($personId), $objectFactory->createGrade(null, $personId, $moduleId), $objectFactory);
        }

        public static function createGradeLine(Person $person, Grade $grade, ObjectFactory $objectFactory)
        {
            return new GradeLine($person, $grade, $objectFactory);
        }

    }
}