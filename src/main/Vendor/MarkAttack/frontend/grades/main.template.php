<?php ob_start(); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="row">
                <div class="page-header">
                    <form id="module-selection" method="post" action="index.php?url=chooseModule">
                        <div class="form-group pull-right">
                            <select id="module" name="module" class="form-control">
                                <?php foreach ($model->getModuleGrades() as $moduleGrade): ?>
                                    <option <?php if ($model->isSelected($moduleGrade)) {
                                        echo 'selected="selected"';
                                    } ?> value="<?php echo Trim($moduleGrade->uniqueName()); ?>"><?php echo Trim($moduleGrade->moduleName()); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </form>
                    <h2>Notes du module <?php echo $model->retrieveSelectedModule()->getModuleName() ?></h2>
                </div>
                <?php
                if (($model->retrieveSelectedModuleGradeLines()->hasGradeLineWithError())) {
                        echo "<div class=\"alert alert-danger\">Les erreurs ci-dessous se produit. Veuillez les corriger ! </div>";
                }
                ?>
                <form method="post" id="grades_form" action="index.php?url=saveGrades">
                    <table id="grades_table" class="table table-bordered">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Nom</th>
                            <th class="text-center">Prenom</th>
                            <th class="text-center">Note</th>
                        </tr>
                        </thead>
                        <tbody style="width: 100%;">
                        <?php
                        $i = 1;
                        foreach ($model->retrieveSelectedModuleGradeLines()->getGradeLines() as $line) {
                            echo "<tr class=\"" . $line->styleClassName() . "\">
                                <td style='width: 5%;' class=\"text-center\">$i</td>
                                <td style='width: 30%;'>" . $line->lastName() . "</td>
                                <td style='width: 30%;'>" . $line->firstName() . "</td>
                                <td style='width: 35%;'>";
                            if ($line->hasWrongGradeValue()) {
                                echo "<div class=\"form-group has-error\">
                                          <input type='text' style='width: 100%;' 
                                            class='grades form-control' name=\"" .
                                    $line->uniqueName() . "\" value=" . $line->gradeValue() .
                                    " ". $line->isRequired() . ">
                                    <span class=\"help-block\">La note doit être entre 0 et 20 !</span>
                                  </div>";
                            } else {
                                echo "<input type='number' step='0.01' max='20' min='0' style='width: 100%;' 
                                class='grades' name=\"" . $line->uniqueName() . "\" value=" . $line->gradeValue() .
                                    " " . $line->isRequired() . ">";
                            }
                            echo "</td>
                            </tr>";
                            $i++;
                        }
                        ?>
                        </tbody>
                    </table>
                    <input type="submit" id="saveGrades" name="button_saveGrades" value="Sauvegarder"
                           class="btn btn-lg center-block">
                </form>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
</div>
<script>
    $(function () {
        $('#module').change(function () {
            $('#module-selection').submit();
        });
    });
</script>

<?php $content = ob_get_clean(); ?>

<?php require_once dirname(__FILE__) . '/../Layout.template.php'; ?>

