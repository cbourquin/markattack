<?php

namespace MarkAttack\frontend\grades {

    use Exception;
    use MarkAttack\entity\Grade;
    use MarkAttack\entity\ObjectFactory;
    use MarkAttack\error\ValidationException;
    use MarkAttack\frontend\model\ModelSettings;
    use MarkAttack\entity\Person;
    use MarkAttack\librairie\Page;


    require_once dirname(__FILE__) . '/../../entity/Grade.php';
    require_once dirname(__FILE__) . '/../../entity/ObjectFactory.php';
    require_once dirname(__FILE__) . '/../../entity/Person.php';
    require_once dirname(__FILE__) . '/../../dao/SQLExpressions.php';
    require_once dirname(__FILE__) . '/../../dao/Db.php';
    require_once dirname(__FILE__) . '/ModuleGradeLines.php';
    require_once dirname(__FILE__) . '/GradesModel.php';
    require_once dirname(__FILE__) . '/ModuleGrades.php';
    require_once dirname(__FILE__) . '/../model/ModelSettings.php';


    class ActionGrades
    {

        public static function initGrades()
        {
            self::startSession();
            if (isset($_SESSION['user'])){
                $user = unserialize($_SESSION['user']);
                switch ($user->FK_Person_Type){
                    case Person::ROLE_ENSEIGNANT:
                        $moduleIds = ModelSettings::getTeacherModuleIds($user->getId());
                        $_SESSION[GradesModel::GRADES] = GradesModel::createGradesModel($moduleIds, new ObjectFactory());
                        Page::display('index.php?url=grades');
                        break;

                    case Person::ROLE_ADMIN:
                        $moduleIds = ModelSettings::getTeacherModuleIds($user->getId());
                        $_SESSION[GradesModel::GRADES] = GradesModel::createGradesModel($moduleIds, new ObjectFactory());
                        Page::display('index.php?url=grades');
                        break;

                    default :
                        Page::display('index.php?url=error-denied');
                        break;
                }
            } else {
                Page::display('index.php?url=index');
            }
        }

        public static function chooseModule()
        {
            self::startSession();
            if (isset($_SESSION['user'])){
                $user = unserialize($_SESSION['user']);
                switch ($user->FK_Person_Type){
                    case Person::ROLE_ENSEIGNANT:
                        $model = self::retrieveGradesModel();
                        if (isset($_POST['module'])) {
                            $module = $_POST['module'];
                            $model->selectModuleGrades(trim($module));
                            Page::display('index.php?url=grades');
                        }
                        break;

                    case Person::ROLE_ADMIN:
                        $model = self::retrieveGradesModel();
                        if (isset($_POST['module'])) {
                            $module = $_POST['module'];
                            $model->selectModuleGrades(trim($module));
                            Page::display('index.php?url=grades');
                        }
                        break;

                    default :
                        Page::display('index.php?url=error-denied');
                        break;
                }
            } else {
                Page::display('index.php?url=index');
            }
        }


        public static function saveGrades()
        {
            self::startSession();
            if (isset($_SESSION['user'])){
                $user = unserialize($_SESSION['user']);
                switch ($user->FK_Person_Type){
                    case Person::ROLE_ENSEIGNANT:
                        $model = self::retrieveGradesModel();
                        $model->saveGrades(function ($line) {
                            return $_POST[$line->uniqueName()];
                        });
                        require_once 'main.template.php';
                        break;

                    case Person::ROLE_ADMIN:
                        $model = self::retrieveGradesModel();
                        $model->saveGrades(function ($line) {
                            return $_POST[$line->uniqueName()];
                        });
                        require_once 'main.template.php';
                        break;

                    default :
                        Page::display('index.php?url=error-denied');
                        break;
                }
            } else {
                Page::display('index.php?url=index');
            }

        }

        public static function showGrades()
        {
            self::startSession();
            if (isset($_SESSION['user'])){
                $user = unserialize($_SESSION['user']);
                switch ($user->FK_Person_Type){
                    case Person::ROLE_ENSEIGNANT:
                        $model = self::retrieveGradesModel();
                        if ($model->hasNoModules()){
                            require_once 'noModules.template.php';
                        }else{
                            require_once 'main.template.php';
                        }
                        break;

                    case Person::ROLE_ADMIN:
                        $model = self::retrieveGradesModel();
                        if ($model->hasNoModules()){
                            require_once 'noModules.template.php';
                        }else{
                            require_once 'main.template.php';
                        }
                        break;

                    default :
                        Page::display('index.php?url=error-denied');
                        break;
                }
            } else {
                Page::display('index.php?url=index');
            }
        }

        private static function retrieveGradesModel()
        {
            if (!isset($_SESSION[GradesModel::GRADES])) {
                self::initGrades();
            }
            return $_SESSION[GradesModel::GRADES];
        }

        private static function startSession()
        {
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
        }
    }
}