<?php

namespace MarkAttack\frontend\grades {


    use MarkAttack\entity\Module;
    use MarkAttack\entity\ObjectFactory;

    class ModuleGrades
    {
        private $module;
        private $moduleGradeLines;
        private $objectFactory;

        public function __construct(Module $module, ObjectFactory $objectFactory)
        {
            $this->module = $module;
            $this->objectFactory = $objectFactory;
        }

        public function getModuleGradeLines()
        {
            if ($this->moduleGradeLines == null) {
                $this->moduleGradeLines = $this->createModuleGradeLines();
            }
            return $this->moduleGradeLines;
        }

        public function getModule()
        {
            return $this->module;
        }

        public function moduleName()
        {
            return $this->module->getModuleName();
        }

        public function uniqueName()
        {
            return $this->module->getId();
        }

        public function createModuleGradeLines()
        {
            $moduleGradeLines = new ModuleGradeLines($this->module, $this->objectFactory);
            return $moduleGradeLines->initializeGradeLineManager();
        }



    }
}