<?php

namespace MarkAttack\frontend\grades {

    use Db;
    use MarkAttack\entity\Grade;
    use MarkAttack\entity\Module;
    use MarkAttack\entity\ObjectFactory;
    use MarkAttack\error\ValidationException;
    use MarkAttack\frontend\model\ModelSettings;

    require_once dirname(__FILE__) . '/GradeLine.php';
    require_once dirname(__FILE__) . '/../../entity/Module.php';
    require_once dirname(__FILE__) . '/../../entity/Grade.php';
    require_once dirname(__FILE__) . '/../../entity/Person.php';
    require_once dirname(__FILE__) . '/../../dao/SQLExpressions.php';
    require_once dirname(__FILE__) . '/../../dao/Db.php';

    class ModuleGradeLines
    {
        public static $MODULE_GRADE_LINES = 'ModuleGradeLines';
        private $gradeLines = array();
        private $module;
        private $objectFactory;

        public function __construct(Module $module, ObjectFactory $objectFactory)
        {
            $this->module = $module;
            $this->objectFactory = $objectFactory;
        }

        public function getGradeLines()
        {
            return $this->gradeLines;
        }

        public function saveGrades(callable $gradeSupplier)
        {
            foreach ($this->gradeLines as $line) {
                $gradeValue = call_user_func($gradeSupplier, $line);
                $line->save($gradeValue);
            }
            return $this;
        }

        public function addGradeLine(GradeLine $gradeLine)
        {
            array_push($this->gradeLines, $gradeLine);
            return $this;
        }

        public function getModule()
        {
            return $this->module;
        }

        public function hasGradeLineWithError(){
            foreach ($this->gradeLines as $line){
                if ($line->hasWrongGradeValue()) {
                    return true;
                }
            }
            return false;
        }

        /************************************
         *  Initialize
         ************************************/
        public function initializeGradeLineManager()
        {
            $moduleGradeIds = ModelSettings::getModuleGradeIds($this->getModule()->getId());
            $allModuleStudentIds = ModelSettings::getModuleStudentIds($this->getModule()->getId());
            $this
                ->fillExistingGradeLines($moduleGradeIds)
                ->fillNewGradeLines($allModuleStudentIds)
                ->sortGradeLines();
            return $this;
        }

        public function fillExistingGradeLines($gradeIds)
        {
            foreach ($gradeIds as $gradeId) {
                array_push($this->gradeLines, (GradeLine::createExistingGradeLine($gradeId, $this->objectFactory)));
            }
            return $this;
        }


        public function retrieveGradeLineHavingPersonId($personId)
        {
            foreach ($this->gradeLines as $line) {
                if ($personId == $line->getPerson()->getId()) {
                    return $line;
                }
            }
            return null;
        }

        public function fillNewGradeLines($personIds)
        {
            foreach ($personIds as $personId) {
                if ($this->retrieveGradeLineHavingPersonId($personId) == null) {
                    array_push($this->gradeLines, GradeLine::createNewGradeLine($personId, $this->module->id, $this->objectFactory));
                }
            }
            return $this;
        }

        public function sortGradeLines()
        {
            usort($this->gradeLines, function (GradeLine $line1, GradeLine $line2) {
                if (($sameLastName = strcmp($line1->lastName(), $line2->lastName())) !== 0) {
                    return $sameLastName;
                }
                return strcmp($line1->firstName(), $line2->firstName());
            });
            return $this;
        }

    }
}