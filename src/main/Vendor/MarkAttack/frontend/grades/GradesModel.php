<?php

namespace MarkAttack\frontend\grades;


use Exception;
use MarkAttack\entity\Module;
use MarkAttack\entity\ObjectFactory;

class GradesModel
{

    private $moduleGrades;
    private $selection;
    const GRADES = "GRADES";
    private $objectFactory;

    private function __construct(ObjectFactory $objectFactory)
    {
        $this->moduleGrades = array();
        $this->objectFactory = $objectFactory;
    }

    public function addModuleGrade(ModuleGrades $moduleGrades){
        array_push($this->moduleGrades, $moduleGrades);
    }

    public function selectModuleGrades( $uniqueName){
        foreach ($this->moduleGrades as $moduleGrade){
            if ($uniqueName==$moduleGrade->uniqueName()){
                $this->selection = $moduleGrade;
            }
        }
    }

    public function retrieveSelectedModuleGradeLines()
    {
        self::handleNoSelection();
       return $this->selection->getModuleGradeLines();
    }


    public function retrieveSelectedModule()
    {
        self::handleNoSelection();
        return $this->selection->getModule();
    }

    public function isSelected(ModuleGrades $moduleGrades){
        self::handleNoSelection();
        return $this->selection->getModule()->getId() === $moduleGrades->getModule()->getId();
    }

    public function getModuleGrades()
    {
        return $this->moduleGrades;
    }

    public static function createGradesModel($moduleIds, ObjectFactory $objectFactory){
        $model = new GradesModel($objectFactory);
        foreach ($moduleIds as $moduleId){
            $model->addModuleGrade(new ModuleGrades($objectFactory->createModule($moduleId), $objectFactory));
        }
        return $model;
    }

    private function handleNoSelection(){
        if (empty($this->moduleGrades)){
            throw new Exception("There is no module !");
        }
        if ($this->selection==null){
            $this->selection = $this->moduleGrades[0];
        }
    }

    public function saveGrades(callable $gradeValueSupplier)
    {
        $gradeLinesManager = self::retrieveSelectedModuleGradeLines();;
        $gradeLinesManager->saveGrades($gradeValueSupplier);
    }

    public function hasNoModules(){
        return empty($this->moduleGrades);
    }


}