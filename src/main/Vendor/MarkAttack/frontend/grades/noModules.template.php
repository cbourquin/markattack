<?php ob_start(); ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="page-header">
                        <h2>Vous avez aucun module ! </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $content = ob_get_clean(); ?>
<?php require_once dirname(__FILE__) . '/../Layout.template.php'; ?>

