<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 03/10/2018
 * Time: 11:54
 */
namespace MarkAttack\frontend\model;


use MarkAttack\entity\Person;
use MarkAttack\entity\Module;
use MarkAttack\dao\Db;
use MarkAttack\librairie\Page;
use PDO;
use SQLExpressions;

class ModelSettings
{
    public static function getTeacherNotList($id){
        $db = new Db();
        $sql = "SELECT PK_Person
                FROM Person INNER JOIN Module_Person ON Module_Person.FK_Person = Person.PK_Person
                WHERE Module_Person.FK_Module <> :id 
                  AND Person.FK_Person_Type = :idPerson
                  AND PK_Person NOT IN (SELECT PK_Person
                                        FROM Person INNER JOIN Module_Person ON Module_Person.FK_Person = Person.PK_Person
                                        WHERE Module_Person.FK_Module = :id 
                                          AND Person.FK_Person_Type = :idPerson)";

        $values = $db->query($sql, array(':id'=>$id, ':idPerson'=>2));
        $population = [];

        foreach ($values as $value){
            $population[] = new Person($value['PK_Person']);
        }
        return $population;
    }

    public static function getStudentNotRegisterToModule($module){
        $db = new Db();
        $result = $db->query("SELECT DISTINCT T01.PK_Person, CONCAT(T01.Person_Lastname,' ',T01.Person_Firstname) as name 
                                  FROM Person T01 
                                    LEFT JOIN Module_Person T02 ON T01.PK_Person = T02.FK_Person and T02.FK_Module != :module
                                  WHERE T01.FK_Person_Type=:tPerson ",
            array(':tPerson'=>Person::ROLE_ETUDIANT, ':module'=>$module), PDO::FETCH_KEY_PAIR);
        return $result;
    }

    public static function getStudentRegisterToModule($module){
        $db = new Db();
        $result = $db->query("SELECT DISTINCT T01.PK_Person, CONCAT(T01.Person_Lastname,' ',T01.Person_Firstname) as name 
                                  FROM Person T01 
                                    INNER JOIN Module_Person T02 ON T01.PK_Person = T02.FK_Person and T02.FK_Module = :module
                                  WHERE T01.FK_Person_Type = :tPerson ",
            array(':tPerson'=>Person::ROLE_ETUDIANT, ':module'=>$module), PDO::FETCH_KEY_PAIR);
        return $result;
    }

    public static function getModules($id=null){
        $db = new Db();
        $result = $db->query("SELECT T01.PK_Module, T01.Module_Name 
                                  ".(($id)?"FROM Module T01 INNER JOIN Module_Person T02 ON T01.PK_Module = T02.FK_Module 
                                            WHERE T02.FK_Person = :user":"FROM Module T01")
                                   . " GROUP BY T01.PK_Module, T01.Module_Name ORDER BY T01.Module_Name",
                        ($id)?array(':user'=>$id):null, PDO::FETCH_KEY_PAIR);
        return $result;
    }

    public static function getEmail(){
        $db = new Db();
        $sql = "SELECT Person_Login, Person_Mail
                FROM Person";

        $result = $db->query($sql, null, PDO::FETCH_KEY_PAIR);
        return $result;
    }

    public static function getProfesseur(){
        $db = new Db();
        $sql = "SELECT PK_Person
                FROM Person 
                WHERE Person.FK_Person_Type = :tPerson";

        $result = $db->query($sql, array('tPerson'=>2), PDO::FETCH_COLUMN);
        return $result;
    }

    public static function getPersons($id){
        $db = new Db();
        $values = $db->query("SELECT DISTINCT Person.PK_Person, Person.Person_FirstName
                                  FROM Person
                                  WHERE Person.PK_Person <> :id ",array('id'=>$id), PDO::FETCH_KEY_PAIR);
        $population = [];
        Page::set('Persons', $values);
        foreach ($values as $key => $value){
            $population[$key] = new Person($key);
        }
        return $population;
    }

    public static function getModules2($id=null){
        $db = new Db();
        $sql = "SELECT T01.PK_Module, GROUP_CONCAT(distinct T03.Person_Lastname)
                FROM Module T01
                  LEFT JOIN Module_Person T02 ON T01.PK_Module = FK_Module
                  LEFT JOIN Person T03 on T02.FK_Person = T03.PK_Person AND FK_Person_Type = 2 "
                  .(($id==null)?'':"WHERE T03.PK_Person = :id ").
                "GROUP BY T01.PK_Module";

        $params = (($id==null)?null:array(':id'=>$id));
        $values = $db->query($sql,$params, PDO::FETCH_KEY_PAIR);
        $subjects = [];
        Page::set('Subjects', array_keys($values));
        foreach ($values as $key => $value){
            $subjects[$key]['class'] = new Module($key);
            $subjects[$key]['teachers'] = $value;
        }
        return $subjects;
    }

    public static function getModuleStudentIds($moduleId){
        $db=new Db();
        $result = $db->getTableWithParams(SQLExpressions::SELECT_STUDENTS_OF_MODULE, array($moduleId, Person::ROLE_ETUDIANT));
        $allModuleStudents = array();
        foreach ($result as  $row){
            array_push($allModuleStudents, $row['FK_Person']);
        }
        return $allModuleStudents;
    }

    public static function getModuleGradeIds($moduleId){
        $db=new Db();
        $result = $db->getTableWithParams(SQLExpressions::SELECT_ALL_GRADES_OF_MODULE, array($moduleId));
        $gradeIds = array();
        foreach ($result as  $row){
            array_push($gradeIds, $row['PK_Grade']);
        }
        return $gradeIds;
    }

    public static function getTeacherModuleIds($userId){
        $db=new Db();
        $result = $db->getTableWithParams(SQLExpressions::SELECT_TEACHER_MODULE, array($userId));
        $moduleIds = array();
        foreach ($result as  $row){
            array_push($moduleIds, $row['FK_Module']);
        }
        return $moduleIds;
    }
}