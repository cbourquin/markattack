<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 03/10/2018
 * Time: 11:54
 */

namespace MarkAttack\frontend\model;

    use MarkAttack\dao\Db;
    use MarkAttack\entity\Person;
    use PDO;

class ModelReport
{
    public static function getStudents($id){
        $db = new Db();
        $sql = "SELECT T01.PK_Person, CONCAT(T01.Person_Lastname,' ',T01.Person_Firstname) as name
                FROM Person T01 "
                .(($id==null)?'':'INNER JOIN Module_Person T02 ON T01.PK_Person = T02.FK_Person 
                                        and T02.FK_Module in (SELECT DISTINCT FK_Module
                                                                FROM Module_Person
                                                                WHERE FK_Person = :id) ').
                "WHERE T01.FK_Person_Type = 1
                GROUP BY T01.PK_Person, CONCAT(T01.Person_Lastname,' ',T01.Person_FirstName)
                ORDER BY CONCAT(T01.Person_Lastname,' ',T01.Person_FirstName)";
        return $db->query($sql, (($id==null)?null:array(':id'=>$id)), PDO::FETCH_KEY_PAIR);
    }


    public static function getSubjectReport($id){
        $db = new Db();
        $sql = "SELECT CONCAT(T01.Person_Lastname, ' ',T01.Person_Firstname) as name,
                        T04.Grade_Value as note
                from Person T01 
                    inner join Module_Person T02 on T01.PK_Person = T02.FK_Person 
                    inner join Module T03 on T02.FK_Module = T03.PK_Module
                    inner join Grade T04 on T04.FK_Module = T03.PK_Module and T01.PK_Person = T04.FK_Student
                where T01.FK_Person_Type = :tPerson 
                    and T03.PK_Module = :id ";

        return $db->query($sql, array(':id'=>$id, ':tPerson'=>Person::ROLE_ETUDIANT), PDO::FETCH_KEY_PAIR);
    }

    public static function getStudentReport($id){
        $db = new Db();
        $sql = "SELECT T03.Module_Name as name,
                        T03.Module_Coefficient as coeff,
                        T04.Grade_Value as note,
                        T05.moyenne,
                        T05.mini,
                        T05.maxi
                from Person T01 
                    inner join Module_Person T02 on T01.PK_Person = T02.FK_Person 
                    inner join Module T03 on T02.FK_Module = T03.PK_Module
                    inner join Grade T04 on T04.FK_Module = T03.PK_Module and T01.PK_Person = T04.FK_Student
                    left join (SELECT T01.PK_Module as id,
                                      MIN(T02.Grade_Value) as mini,
                                      MAX(T02.Grade_Value) as maxi,
                                      AVG(T02.Grade_Value) as moyenne
                                FROM Module T01 
                                    LEFT JOIN Grade T02 ON T01.PK_Module = T02.FK_Module
                                GROUP BY T01.PK_Module) T05 ON T05.id = T03.PK_Module
                where T01.FK_Person_Type = :tPerson 
                    and T01.PK_Person = :id ";

        return $db->query($sql, array(':id'=>$id, ':tPerson'=>Person::ROLE_ETUDIANT));
    }


}