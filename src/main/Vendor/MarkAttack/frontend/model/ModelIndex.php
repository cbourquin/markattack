<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 09/10/2018
 * Time: 11:09
 */

namespace MarkAttack\frontend\model;

use MarkAttack\dao\Db;
use MarkAttack\entity\Person;
use PDO;

class ModelIndex
{
    public static function getUserByLogin($login){
        $db = new Db();
        $statement =  $db->query("SELECT * FROM Person WHERE Person_Login = :login  LIMIT 1 ",
                            array('login'=>$login), PDO::FETCH_ASSOC);
        if ($statement)
            return new Person($statement[0]['PK_Person']);
        return false;
    }
}