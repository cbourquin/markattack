<?php
/**
 *
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 03/10/2018
 * Time: 11:54
 */

use MarkAttack\dao\Db;
use MarkAttack\entity\Person;
class ModelHome
{
    public static function getReport($id){
        $db = new Db();
        $sql = "select 
                    t1.Module_Name, t1.Module_Coefficient, 
                    GROUP_CONCAT(distinct t3.Person_Lastname) as Teachers , 
                    AVG(t5.Grade_Value) as Averages
                from Module t1 
                    inner join Module_Person t2 on t1.PK_Module = t2.FK_Module 
                    inner join Person t3 on t2.FK_Person = t3.PK_Person and t3.FK_Person_Type != :tPerson 
                    left join Grade t5 on t5.FK_Module = t1.PK_Module
                where t3.PK_Person = :id 
                group by t1.Module_Name, t1.Module_Coefficient";

        return $db->query($sql, array(':id'=>$id, ':tPerson'=>Person::ROLE_ETUDIANT));
    }

    public static function getReporttoAdmin(){
        $db = new Db();
        $sql = "SELECT 
                  T01.Module_Name, T01.Module_Coefficient, 
                  GROUP_CONCAT(distinct T03.Person_Lastname) as Teachers, 
                  AVG(T05.Grade_Value) as Averages
                FROM Module T01 
                    LEFT JOIN Module_Person T02 on T01.PK_Module = T02.FK_Module 
                    LEFT JOIN Person T03 on T02.FK_Person = T03.PK_Person and T03.FK_Person_Type != :tPerson
                    LEFT JOIN Grade T05 on T05.FK_Module = T01.PK_Module
                GROUP BY 
                  T01.Module_Name, T01.Module_Coefficient";

        return $db->query($sql, array(':tPerson'=>Person::ROLE_ETUDIANT));
    }
}