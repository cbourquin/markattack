<div class="row">
    <div class="page-header">
        <h2>Mes notes</h2>
    </div>
    <table id="student-assess" class="table table-bordered">
        <thead>
            <tr>
                <th class="text-center col-sm-6">Matière</th>
                <th class="text-center col-sm-1">Coefficient</th>
                <th class="text-center col-sm-1">Note</th>
                <th class="text-center col-sm-1">Minimum</th>
                <th class="text-center col-sm-1">Maximum</th>
                <th class="text-center col-sm-2">Appréciation</th>
            </tr>
        </thead>
        <tbody>
        <?php $moyenne=0; $coef=0;?>
        <?php foreach ($grades as $grade): ?>
            <tr>
                <?php $module = $grade->getModule();?>
                <?php $all = $grade->getAllGradeOfModule();?>
                <td class="text-left"><?php echo $module->Module_Name;?></td>
                <td class="text-center"><?php echo round($module->Module_Coefficient,1);?></td>
                <td class="text-center"><?php echo round($grade->Grade_Value,1); ?></td>
                <td class="text-center"><?php echo round(min($all),1);?></td>
                <td class="text-center"><?php echo round(max($all),1);?></td>
                <td class="text-center"><?php if($grade->Grade_Value>=10):?><i class="glyphicon glyphicon-thumbs-up smiley"></i><?php else:?><i class="glyphicon glyphicon-thumbs-down hungry"></i><?php endif;?></td>
                <?php $moyenne = $moyenne + ($module->Module_Coefficient * $grade->Grade_Value);?>
                <?php $coef = $coef + $module->Module_Coefficient;?>
            </tr>
        <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="1" class="text-center">Moyenne</th>
                <th colspan="3" class="text-center"><?php echo round(($moyenne/$coef),1);?></th>
                <th colspan="2" class="text-center"><?php if(($moyenne/$coef)>=10):?><i class="glyphicon glyphicon-thumbs-up smiley"></i><?php else:?><i class="glyphicon glyphicon-thumbs-down hungry"></i><?php endif;?></th>
            </tr>
        </tfoot>
    </table>
</div>
<script>$('#student-assess').DataTable();</script>