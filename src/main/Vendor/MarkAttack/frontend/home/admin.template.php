<?php use \MarkAttack\entity\Person;?>
<div class="row">
    <div class="page-header">
        <h2>Mes Modules</h2>
    </div>
    <table id="tablehome" class="table table-bordered">
        <thead>
        <tr>
            <th class="text-center">Nom du module</th>
            <th class="text-center">Coefficient</th>
            <th class="text-center">Enseignants</th>
            <th class="text-center">Moyenne</th>
            <th class="text-center">Reussite</th>
        </tr>
        </thead>
        <tbody>
            <?php foreach ($mods as $row => $innerArray) : ?>
                <tr>
                    <td class="text-left"><?php echo $innerArray["Module_Name"];?></td>
                    <td class="text-center"><?php echo $innerArray["Module_Coefficient"];?></td>
                    <td class="text-left"><?php echo $innerArray["Teachers"];?></td>
                    <td class="text-center"><?php echo ($innerArray["Averages"])?round($innerArray["Averages"],1):'';?></td>
                    <td class="text-center"><?php if($innerArray["Averages"]>=10):?><i class="glyphicon glyphicon-thumbs-up smiley"><span class="hidden">1</span></i><?php else:?><i class="glyphicon glyphicon-thumbs-down hungry"><span class="hidden">0</span></i><?php endif;?></td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>


</div>

<script>
    $('#tablehome').DataTable();
</script>