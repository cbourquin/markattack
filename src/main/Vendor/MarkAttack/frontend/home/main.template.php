<?php ob_start(); ?>
    <style>
        .smiley {
            font-size:24px;
            color:green;
        }
        .hungry{
            font-size:24px;
            color:red;
        }
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">
                <?php
                    use MarkAttack\entity\Person;

                    if(unserialize($_SESSION['user'])->FK_Person_Type==Person::ROLE_ETUDIANT)
                        require_once "student.template.php";
                    else
                        require_once "admin.template.php";
                ?>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
<?php $content = ob_get_clean(); ?>

<?php require_once dirname(__FILE__) . '/../Layout.template.php'; ?>