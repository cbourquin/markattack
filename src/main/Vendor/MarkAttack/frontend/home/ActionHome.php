<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 24/09/2018
 * Time: 23:24
 */

require_once "frontend/errs/ActionErrors.php";
require_once "frontend/model/ModelHome.php";

use MarkAttack\entity\Person;
use MarkAttack\librairie\Page;

class ActionHome
{
    public static function accueil(){
        session_start();
        if (isset($_SESSION['user'])){
            $p = unserialize($_SESSION['user']);
            switch ($p->FK_Person_Type){
                case Person::ROLE_ETUDIANT:
                    $grades = $p->getGrades();
                    break;
                case Person::ROLE_ENSEIGNANT:
                    $mods = ModelHome::getReport($p->id);
                    break;
                case Person::ROLE_ADMIN:
                    $mods = ModelHome::getReporttoAdmin();
                    break;
            }
            require_once "main.template.php";
        }else{
            Page::display('index.php?url=index');
        }
    }

}