<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 24/09/2018
 * Time: 23:24
 */

namespace MarkAttack\frontend\index;

require_once 'librairie/Page.php';
require_once 'librairie/Form.php';
require_once 'frontend/model/ModelIndex.php';


use MarkAttack\librairie\Page;
use MarkAttack\librairie\Form;
use MarkAttack\entity\Person;
use MarkAttack\frontend\model\ModelIndex;


    class MainAction
    {

        public static function authentification(){
            session_start();
            if (isset($_SESSION['user'])) {
                Page::display('index.php?url=home');
            }
            require_once 'main.template.php';
        }

        public static function verification()
        {
            session_start();
            $res = Form::Authentification();

            if ($res!=false) {
                $person = ModelIndex::getUserByLogin($res['pseudo']);
                if($person!=false) {
                    if (preg_match("/\bpreprod\b/i", $_SERVER['REQUEST_URI']))
                        $pwd_valid = ($res['pwd'] === $person->Person_Password);
                    else
                        $pwd_valid = password_verify($res['pwd'], $person->Person_Password);

                    if ($pwd_valid) {
                        $_SESSION['user'] = serialize($person);
                        Page::display('index.php?url=home');
                    }
                }

                $_SESSION['errors'] = "Login ou mot de passe érroné";
                Page::displayErrs('index.php?url=index');

            }
            Page::displayErrs('index.php?url=index');
        }

        public static function deconnexion()
        {
            session_start();
            session_unset();
            session_destroy();
            Page::display('index.php?url=index');
        }
}