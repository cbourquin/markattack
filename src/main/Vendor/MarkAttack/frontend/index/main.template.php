<?php ob_start(); ?>

<div class="container-fluid">
    <?php if (isset($_SESSION['errors'])):?>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                Erreur: <?php echo $_SESSION['errors'];?>
            </div>
        </div>
        <div class="col-sm-2"></div>
    </div>
    <?php endif; ?>

	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h5><strong>Authentification</strong></h5>
				</div>
				<div class="panel-body">
					<form class="form-horizontal" action="index.php?url=verify-identity" method="POST">
					
						<div class="form-group">
							<label class="control-label col-sm-4" for="pseudo">Identifiant: </label>
							<div class="col-sm-7">
								<input type="text" class="form-control" name="pseudo" id="pseudo" placeholder="Pseudonyme">
							</div>
						</div>
						
						<div class="form-group">
							<label class="control-label col-sm-4" for="pwd">Mot de passe: </label>
							<div class="col-sm-7">
								<input type="password" class="form-control" name="pwd" id="pwd" placeholder="Mot de passe">
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-9">
                                <button type="submit" class="btn btn-default pull-right">Connexion <i class="glyphicon glyphicon-log-out"></i></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $content = ob_get_clean(); ?>
<?php require __DIR__ . '/../Layout.template.php'; ?>