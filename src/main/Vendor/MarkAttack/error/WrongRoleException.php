<?php
/**
 * Created by PhpStorm.
 * User: juju3
 * Date: 28/09/2018
 * Time: 21:58
 */

class WrongRoleException extends Exception
{
    /**
     * @param $RoleExpected Role attendue mais pas trouvée
     */
    public function setMessage($RoleExpected){
        switch ($RoleExpected){
            case Personne::ROLE_Etudiant:
                $this->message = "Cette ressources concerne uniquement les étudiants";
                break;
            case Personne::ROLE_Admin:
                $this->message = "Vous n'avez pas les privilèges administrateur";
                break;
            case Personne::ROLE_Admin:
                $this->message = "Seul les ensignants ont accès à cette ressource";
                break;
            default:
                $this->message = "Vous n'avez pas les droits nécessaires";
                break;
        }
    }
}