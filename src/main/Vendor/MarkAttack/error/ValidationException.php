<?php
/**
 * Created by PhpStorm.
 * User: juju3
 * Date: 16/10/2018
 * Time: 11:02
 */

namespace MarkAttack\error {
    use Exception;
    class ValidationException extends Exception
    {
        private $errors;
        public function __construct($errors)
        {
            $this->errors = $errors;
            $msg = "";
            $count = 0;
            foreach ($errors as $key => $error) {
                if($count != 0)
                    $msg .= ', ';
                $count++;
                $msg .= "$key  -> $error";
            }
            $this->message = $msg;
        }

        public function getErrors(){
            return $this->errors;
        }

    }
}