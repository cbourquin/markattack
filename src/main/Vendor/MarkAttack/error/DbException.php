<?php
/**
 * Created by PhpStorm.
 * User: juju3
 * Date: 02/10/2018
 * Time: 11:44
 */
namespace MarkAttack\error {
    use Exception;
    class DbException extends Exception
    {
        public function __construct($msg = null)
        {
            $this->message = $msg;
        }

        /**
         * @param sql $errors
         */
        public function setMessage($errors)
        {
            $msg = "";
            foreach ($errors as $error) {
                $msg .= $error;
            }
            $this->message = $msg;
        }
    }
}