<?php

    require_once 'frontend/grades/ActionGrades.php';
    require_once 'frontend/settings/ActionSettings.php';
    require_once 'frontend/configuration/ActionConfiguration.php';
    require_once 'frontend/index/MainAction.php';
    require_once 'frontend/home/ActionHome.php';
    require_once 'frontend/reporting/ActionReport.php';
    require_once 'configuration/Router.php';

    use MarkAttack\configuration\Router;
    use MarkAttack\frontend\index\MainAction;
    use MarkAttack\frontend\grades\ActionGrades;
    use MarkAttack\frontend\settings\ActionSettings;
    use MarkAttack\frontend\reporting\ActionReport;
    use MarkAttack\frontend\configuration\ActionConfiguration;


/**
     * Vérification de l'url
     */

    if (isset($_GET['url'])){
        $router = new Router($_GET['url']);
    }else{
        $router = new Router('');
    }
    /**
     * Definition des différentes routes du site
     */

    /*********************************************
     *  Module Erreur 404 ou Accés non autorisé
     *********************************************/
        $router->get('error-404', function(){
            ActionErrors::errs404();
        });

        $router->get('error-denied', function(){
            ActionErrors::errsDenied();
        });

        $router->get('error-maintenance', function(){
            ActionErrors::errsMaintenance();
        });

    /*********************************************
     *              Module Index
     *********************************************/
    $router->get('', function(){
        MainAction::authentification();});

    $router->get('index', function(){
        MainAction::authentification();
    });

    $router->post('verify-identity', function(){
        MainAction::verification();
    });


    $router->get('log-out', function(){
        MainAction::deconnexion();
    });

    /*********************************************
     *              Module Home
     *********************************************/

    $router->get('home', function(){
        ActionHome::accueil();
    });

    /*********************************************
     *              Module Settings
     *********************************************/

    /**********************Gestion de l'ajout/Suppression d'un etudiant d'un module*****************************/

    $router->get('settings-student-subject-{id}', function($id){
        ActionConfiguration::configModules($id);
    });

    $router->get('settings-add-student-{student}-subject-{subject}', function($student, $subject){
        ActionConfiguration::ajoutModules($student, $subject);
    });

    $router->get('settings-delete-student-{student}-subject-{subject}', function($student, $subject){
        ActionConfiguration::supprimeModules($student, $subject);
    });

    /**********************Gestion de l'ajout/Suppression d'un prof d'un module**********************************/


    $router->get('settings-teacher-subject-{id}', function($id){
        ActionConfiguration::ajoutProfModule($id);
    });

    $router->get('settings-add-teacher-{teacher}-subject-{subject}', function($teacher, $subject){
        ActionConfiguration::ajoutProf($teacher, $subject);
    });

    $router->get('settings-remove-teacher-{teacher}-subject-{subject}', function($teacher, $subject){
        ActionConfiguration::deleteProf($teacher, $subject);
    });


    /*********************************************
     *             Module Paramètre Logiciel
     *********************************************/

    /**********************Gestion utilisateur****************************************************************/

    $router->get('settings-user-{user}', function($user){
        ActionSettings::gestionUtilisateur($user);
    });

    $router->post('settings-add-user', function(){
        ActionSettings::ajouterUtilisateur();
    });

    $router->post('settings-update-user-{user}', function($user){
        ActionSettings::modifierUtilisateur($user);
    });

    $router->get('settings-delete-user-{user}', function($user){
        ActionSettings::supprimerUtilisateur($user);
    });

    /**********************Gestion module**************************************************************************/

    $router->get('settings-subject-{subject}', function($subject){
        ActionSettings::gestionModule($subject);
    });

    $router->post('settings-add-subject', function(){
        ActionSettings::ajouterModule();
    });

    $router->post('settings-update-subject-{subject}', function($subject){
        ActionSettings::modifierModule($subject);
    });

    $router->get('settings-delete-subject-{subject}', function($subject){
       ActionSettings::supprimerModule($subject);
    });



    /*********************************************
     *              Module ActionGrades
     *********************************************/

    $router->get('initGrades', function(){
        ActionGrades::initGrades();
    });

    $router->get('grades', function(){
        ActionGrades::showGrades();
    });
    $router->post('saveGrades', function(){
        ActionGrades::saveGrades();
    });
    $router->post('chooseModule', function(){
        ActionGrades::chooseModule();
    });

    /*********************************************
    *              Module Reporting
    *********************************************/

    $router->get('reporting-subject-{id}', function($id){
        ActionReport::reportSubject($id);
    });

    $router->get('reporting-student-{id}', function($id){
        ActionReport::reportStudent($id);
    });

    /**
     * Lancer le routeur
     */
    $router->run();

?>