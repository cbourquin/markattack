<?php
namespace MarkAttack\common {
    class Tools
    {
        public static function escape($string, $html_ok = false, $bq_sql = false)
        {
            if (!is_numeric($string)) {
                if (!$html_ok)
                    $string = strip_tags(Tools::nl2br($string));
                if ($bq_sql === true)
                    $string = str_replace('`', '\`', $string);
            }
            return $string;
        }

        public static function redirect($page)
        {
            $lastOccuence = strrpos($_SERVER['PHP_SELF'], '/');
            $url = 'http://' . $_SERVER['SERVER_NAME'] . substr($_SERVER['PHP_SELF'], 0, $lastOccuence) . '/' . $page;
            header('Location: ' . $url);
            exit;
        }

        public static function nl2br($str)
        {
            return str_replace(array("\r\n", "\r", "\n"), '<br />', $str);
        }
    }
}
?>