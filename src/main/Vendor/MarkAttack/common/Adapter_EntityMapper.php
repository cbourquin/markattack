<?php
namespace MarkAttack\common {
    use MarkAttack\dao\Db;
    class Adapter_EntityMapper
    {
        /**
         * Load ObjectModel
         * @param $id
         * @param $id_lang
         * @param $entity ObjectModel
         * @param $entity_defs
         * @param $id_shop
         * @param $should_cache_objects
         */
        public function load($id, $entity, $entity_defs)
        {
            // Load object from database if object id is present
            $sql = 'SELECT * FROM ' . $entity_defs['table'] . ' a WHERE a.`' . $entity_defs['primary'] . '` = ' . (int)$id;
            $db = $entity->getDb();
            // Get lang informations
            if ($object_datas = $db->getRow($sql)) {
                $entity->id = (int)$id;
                foreach ($object_datas as $key => $value) {
                    if (array_key_exists($key, $entity)) {
                        $entity->{$key} = $value;
                    }
                }
            }
        }
    }
}
