<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 24/09/2018
 * Time: 20:59
 */
namespace MarkAttack\configuration;

    use MarkAttack\librairie\Page;
    require_once 'Route.php';

class Router
{

    private $url;
    private $routes = [];

    public function __construct($url)
    {
        $this->url = $url;
    }

    public function get($path, $callable)
    {
        return $this->add($path, $callable, 'GET');
    }

    public function post($path, $callable)
    {
        return $this->add($path, $callable, 'POST');
    }

    private function add($path, $callable, $method)
    {
        $route = new Route($path, $callable);
        $this->routes[$method][] = $route;
        return $route;
    }


    public function run()
    {
        if (!isset($this->routes[$_SERVER['REQUEST_METHOD']])) {
            echo 'REQUEST_METHOD does not exist: ';
        } else {
            foreach ($this->routes[$_SERVER['REQUEST_METHOD']] as $route) {
                if ($route->match($this->url)) {
                    return $route->call();
                }
            }
            Page::display('index.php?url=error-404');
        }
    }

}