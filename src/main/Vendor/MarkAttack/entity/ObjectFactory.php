<?php


namespace MarkAttack\entity {


    class ObjectFactory
    {
        public function createModule($id = null, $name = null, $coeff = null)
        {
            return new Module($id, $name, $coeff);
        }

        public function createGrade($id = null, $id_personne = null, $id_module = null, $note = null)
        {
            return new Grade($id, $id_personne, $id_module, $note);
        }

        public function createPerson($id = null, $nom = null, $prenom = null, $person_type = null, $mail = null, $login = null)
        {
            return new Person($id, $nom, $prenom, $person_type, $mail, $login);
        }

    }
}