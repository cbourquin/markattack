<?php

namespace MarkAttack\entity {

    require_once dirname(__FILE__) . '/ObjectModel.php';

    use mageekguy\atoum\asserters\boolean;
    use MarkAttack\dao\Db;
    class Module extends ObjectModel
    {
        public $Module_Coefficient;
        public $Module_Name;

        public static $definition = array(
            'table' => 'Module',
            'primary' => 'PK_Module',
            'fields' => array(
                /* Classic fields */
                'Module_Name' => array('type' => self::TYPE_STRING, 'validate' => 'iscleanhtml'),
                'Module_Coefficient' => array('type' => self::TYPE_FLOAT,'validate'=> 'islimited','bornes' => array(
                    'options' => array( 'min_range' => 0)))
            )
        );

        public function __construct($id=null,$name=null,$coeff=null)
        {
            parent::__construct($id);
            if($coeff != null)
                $this->Module_Coefficient = $coeff;
            if($name != null)
                $this->Module_Name = $name;
        }

        public function getStudent(){
            $db = new Db();
            $etudiants = $db->query("SELECT FK_Person 
                                      FROM Module_Person m 
                                        INNER JOIN Person p ON p.PK_Person = m.FK_Person 
                                      WHERE p.FK_Person_Type = ".Person::ROLE_ETUDIANT." 
                                        AND m.FK_Module = ".$this->id);
            $students = [];
            if($etudiants) {
                foreach ($etudiants as $etudiant) {
                    $students[] = new Person($etudiant["FK_Person"]);
                }
            }
            return $students;
        }

        public function getNotStudent(){
            $db = new Db();
            $sql = "SELECT  p.PK_Person
                FROM    Person p
                  LEFT JOIN Module_Person mp ON mp.FK_Person = p.PK_Person AND mp.FK_Module = :id
                WHERE   mp.FK_Person IS NULL AND p.FK_Person_Type = :idPerson";
            $values = $db->query($sql, array(':id'=>$this->id, ':idPerson'=>Person::ROLE_ETUDIANT));
            $population = [];

            foreach ($values as $value){
                $population[] = new Person($value['PK_Person']);
            }
            return $population;
        }

        public function getTeacher(){
            $db = new Db();

            $sql = "SELECT DISTINCT PK_Person
                FROM Module_Person INNER JOIN Person ON Module_Person.FK_Person = Person.PK_Person
                WHERE Person.FK_Person_Type = :idPerson
                  AND FK_Module = :id";

            $values = $db->query($sql, array(':id'=>$this->id, ':idPerson'=>Person::ROLE_ENSEIGNANT));
            $population = [];

            foreach ($values as $value){
                $population[] = new Person($value['PK_Person']);
            }
            return $population;
        }

        public function getTeacherNotList(){
            $db = new Db();

            $sql = "SELECT  p.PK_Person
                FROM    Person p
                  LEFT JOIN Module_Person mp ON mp.FK_Person = p.PK_Person AND mp.FK_Module = :id
                WHERE   mp.FK_Person IS NULL AND p.FK_Person_Type = :idPerson";

            $values = $db->query($sql, array(':id'=>$this->id, ':idPerson'=>Person::ROLE_ENSEIGNANT));
            $population = [];

            foreach ($values as $value){
                $population[] = new Person($value['PK_Person']);
            }
            return $population;
        }

        /**
         * Ajoute une personne à un module
         * @param $person
         * @return string
         * @throws DbException
         */
        public function addPerson($person){
            $db = new Db();
            if(!$person instanceof Person)
                $person = new Person($person);
            if(!$person->isInModule($this->id)) {
                return $db->insert("Module_Person", array("FK_Person" => $person->id, "FK_Module" => $this->id));
            }
        }

        public function delPerson($person){
            $db = new Db();
            if(!$person instanceof Person)
                $person = new Person($person);
            if($person->isInModule($this->id)) {
                return $db->delete("Module_Person", array("FK_Person" => $person->id, "FK_Module" => $this->id));
            }
        }


        public function getModuleName()
        {
            return $this->Module_Name;
        }

    }
}