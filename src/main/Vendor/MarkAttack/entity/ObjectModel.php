<?php

namespace MarkAttack\entity {

    use MarkAttack\common\Adapter_EntityMapper;
    use MarkAttack\dao\Db;
    use ReflectionClass;
    use MarkAttack\common\Tools;
    use PDO;
    use MarkAttack\error\ValidationException;

    require_once dirname(__FILE__) . '/../dao/Db.php';
    require_once dirname(__FILE__) . '/../common/Adapter_EntityMapper.php';
    require_once dirname(__FILE__) . '/../common/Tools.php';
    require_once dirname(__FILE__) . '/../error/ValidationException.php';

    abstract class ObjectModel
    {

        const TYPE_INT = 1;
        const TYPE_BOOL = 2;
        const TYPE_STRING = 3;
        const TYPE_FLOAT = 4;
        const TYPE_DATE = 5;
        const TYPE_NOTHING = 6;
        const TYPE_SQL = 7;
        const TYPE_PASSWORD = 8;

        public $id;
        protected static $loaded_classes = array();
        private $db;

        /** @var array Contains current object definition. */
        protected $def;
        /** @var Db An instance of the db in order to avoid calling Db::getInstance() thousands of times. */
        public static $definition = array();

        public function __construct($id = null)
        {
            $class_name = get_class($this);
            $this->def = ObjectModel::getDefinition($class_name);
            $this->setDb(new Db());
            if ($id) {
                $entity_mapper = new Adapter_EntityMapper();
                $entity_mapper->load($id, $this, $this->def);
            }
        }

        public function __sleep()
        {
            $fields = [];
            $class_name = get_class($this);
            $this->def = ObjectModel::getDefinition($class_name);
            foreach ($this->def['fields'] as $key => $value){
                $fields[] = $key;
            }
            $fields[] = 'id';
            $fields[] = 'def';
            return $fields;
        }

        public function __wakeup()
        {
            $this->setDb(new Db());
        }


        public function setDb($db){
            $this->db = $db;
        }
        public function getDb(){
            return $this->db;
        }

        /**
         * Prepare fields for ObjectModel class (add, update)
         * All fields are verified (pSQL, intval, ...)
         *
         * @return array All object fields
         */
        public function getFields()
        {

            $fields = $this->formatFields();
            $this->validateFields();

            // Ensure that we get something to insert
            if (!$fields && isset($this->id)) {
                $fields[$this->def['primary']] = $this->id;
            }

            return $fields;
        }

        public function save()
        {
            return (int)$this->id > 0 ? $this->update() : $this->add();
        }

        public function update()
        {
            if (!$result = $this->db->update($this->def['table'], $this->getFields(), '`' . Tools::escape($this->def['primary']) . '` = ' . (int)$this->id, 0)) {
                return false;
            }
        }

        public function remove()
        {
            return $this->db->delete($this->def['table'], array($this->def['primary'] => $this->id));
        }

        public function add()
        {
            if (!$result = $this->db->insert($this->def['table'], $this->getFields())) {
                return false;
            }
            // Get object id in database
            $this->id = $result;
            return $result;
        }

        /**
         * Formats values of each fields.
         *
         * @since 1.5.0.1
         * @param int $type FORMAT_COMMON or FORMAT_LANG or FORMAT_SHOP
         * @param int $id_lang If this parameter is given, only take lang fields
         *
         * @return array
         */
        public function formatFields()
        {
            $fields = array();
            // Set primary key in fields
            if (isset($this->id)) {
                $fields[$this->def['primary']] = $this->id;
            }
            foreach ($this->def['fields'] as $field => $data) {

                $value = $this->$field;
                // Format field value
                $fields[$field] = ObjectModel::formatValue($value, $data['type'], true, !empty($data['allow_null']));
            }

            return $fields;
        }

        /**
         * Formats a value
         *
         * @param mixed $value
         * @param int $type
         * @param bool $with_quotes
         * @param bool $purify
         * @param bool $allow_null
         * @return mixed
         */
        public static function formatValue($value, $type, $with_quotes = false, $allow_null = false)
        {
            if ($allow_null && $value === null) {
                return array('type' => 'sql', 'value' => 'NULL');
            }
            switch ($type) {
                case self::TYPE_INT:
                    return (int)$value;

                case self::TYPE_BOOL:
                    return (int)$value;

                case self::TYPE_FLOAT:
                    return (float)str_replace(',', '.', $value);

                case self::TYPE_DATE:
                    if (!$value) {
                        return '0000-00-00';
                    }

                    if ($with_quotes) {
                        return '\'' . Tools::escape($value) . '\'';
                    }
                    return Tools::escape($value);

                case self::TYPE_SQL:
                    if ($with_quotes) {
                        return '\'' . Tools::escape($value, true) . '\'';
                    }
                    return Tools::escape($value, true);

                case self::TYPE_PASSWORD:
                    if ($with_quotes) {
                        return '\'' . password_hash($value,PASSWORD_DEFAULT) . '\'';
                    }
                    return password_hash($value,PASSWORD_DEFAULT);
                case self::TYPE_STRING:
                default :
                    if ($with_quotes) {
                        return '\'' . Tools::escape($value, true) . '\'';
                    }
                    return Tools::escape($value);
            }
        }

        public function validateFields(){
            $errors = [];
            foreach ($this->def['fields'] as $field => $data) {
                $value = $this->$field;
                switch ($data['type']){
                    case ObjectModel::TYPE_BOOL:
                        if(!is_bool($value))
                            $errors[$field] = "La valeur n'est pas un booléen";
                        break;
                    case ObjectModel::TYPE_INT:
                        if(!filter_var(intval($value), FILTER_VALIDATE_INT))
                            $errors[$field] = "La valeur n'est pas un entier";
                        break;
                    case ObjectModel::TYPE_FLOAT:
                        if(!filter_var(floatval($value), FILTER_VALIDATE_FLOAT))
                            $errors[$field] = "La valeur n'est pas un nombre décimal";
                        break;

                }
                if(array_key_exists('validate',$data)) {
                    $validation = $data['validate'];
                    switch ($validation){
                        case 'isvalidtype':
                            $types = $this->db->query("SELECT PK_Person_Type FROM `Person_Type`",null,PDO::FETCH_COLUMN);
                            if(!in_array($value,$types))
                                $errors[$field] = "Le type de la personne n'est pas valide $value doit être l'une des suivantes".implode(',',$types);
                            break;
                        case 'isvalidemail':
                            if(!filter_var($value, FILTER_VALIDATE_EMAIL))
                                $errors[$field] ="L'email de la personne n'est pas valide";
                            break;
                        case 'exist':
                            $res = $this->db->query("SELECT * FROM ". $data['field']['table']." WHERE ".$data['field']['column']." = $value");
                            if(!$res) {
                                $errors[$field] = "Une des valeurs n'éxiste pas " . $data['field']['table'];
                            }
                            break;
                        case 'islimited':
                            if(array_key_exists('bornes',$data)) {
                                if (filter_var(intval($value), FILTER_VALIDATE_INT, $data['bornes']) === FALSE) {
                                    $msg = "La valeur saisie n'est pas bonne : $value";
                                    if (isset($data['bornes']['options']['min_range']) && isset($data['bornes']['options']['max_range'])) {
                                        $msg .= ", elle doit être comprise entre " . $data['bornes']['options']['min_range'] . " et " . $data['bornes']['options']['max_range'];
                                    } elseif (isset($data['bornes']['options']['min_range'])) {
                                        $msg .= ", elle doit être supérieur à " . $data['bornes']['options']['min_range'];
                                    } elseif (isset($data['bornes']['options']['max_range'])) {
                                        $msg .= ", elle doit être inférieur à " . $data['bornes']['options']['min_range'];
                                    }
                                    $errors[$field] = $msg;
                                }
                            }
                            break;
                        default:

                            break;
                    }
                }
                if($data['type'] == self::TYPE_STRING){
                    if(strlen($value) > 99){
                        $errors[$field] = "Le nombre de caractères est limité à 99 caractères";
                    }
                }
                if(!isset($value) || count($value) == 0 || strlen($value) == 0 || empty($value))
                    $errors[$field] = "Champs vide";
            }
            if(count($errors) > 0){
                throw new ValidationException($errors);
            }
        }

        /**
         * Returns object definition
         *
         * @param string $class Name of object
         * @param string|null $field Name of field if we want the definition of one field only
         *
         * @return array
         */
        public static function getDefinition($class, $field = null)
        {
            $reflection = new ReflectionClass($class);

            if (!$reflection->hasProperty('definition')) {
                return false;
            }

            $definition = $reflection->getStaticPropertyValue('definition');

            $definition['classname'] = $class;

            if ($field) {
                return isset($definition['fields'][$field]) ? $definition['fields'][$field] : null;
            }

            return $definition;

        }

        public function getId()
        {
            return $this->id;
        }
    }
}

?>