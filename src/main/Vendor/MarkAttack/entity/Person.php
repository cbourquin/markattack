<?php

namespace MarkAttack\entity {
    require_once dirname(__FILE__) . '/ObjectModel.php';
    require_once dirname(__FILE__) . '/Module.php';
    require_once dirname(__FILE__) . '/Grade.php';
    use MarkAttack\dao\Db;
    class Person extends ObjectModel
    {
        const ROLE_ADMIN = 3;
        const ROLE_ETUDIANT = 1;
        const ROLE_ENSEIGNANT = 2;

        public $FK_Person_Type;
        public $Person_Lastname;
        public $Person_Firstname;
        public $Person_Mail;
        public $Person_Login;
        public $Person_Password;

        public static $definition = array(
            'table' => 'Person',
            'primary' => 'PK_Person',
            'fields' => array(
                /* Classic fields */
                'Person_Lastname' => array('type' => self::TYPE_STRING),
                'Person_Firstname' => array('type' => self::TYPE_STRING),
                'FK_Person_Type' => array('type' => self::TYPE_INT,'validate' => 'isvalidtype'),
                'Person_Mail' => array('type' => self::TYPE_STRING,'validate'=>'isvalidemail'),
                'Person_Login' => array('type' => self::TYPE_STRING),
                'Person_Password' => array('type'=>self::TYPE_PASSWORD),
            ),
        );

        public function __construct($id = null, $nom = null, $prenom = null, $person_type = null, $mail = null, $login = null,$mdp = null)
        {
            parent::__construct($id);
            if (isset($nom))
                $this->Person_Lastname = $nom;
            if (isset($prenom))
                $this->Person_Firstname = $prenom;
            if (isset($person_type))
                $this->FK_Person_Type = $person_type;
            if (isset($mail))
                $this->Person_Mail = $mail;
            if (isset($login))
                $this->Person_Login = $login;
            if(isset($mdp))
                $this->Person_Password = $mdp;
        }

        public function getModules()
        {
            $db = new Db();
            $modsId = $db->query("SELECT FK_Module 
                                  FROM Module_Person 
                                  WHERE FK_Person = $this->id");
            $modules = [];
            foreach ($modsId as $modId) {
                $modules[] = new Module($modId['FK_Module']);
            }
            return $modules;
        }

        public function getGrades()
        {
            $db = new Db();
            $gradesId = $db->query("SELECT PK_Grade FROM Grade WHERE FK_Student = $this->id");
            $grades = [];
            foreach ($gradesId as $gradeId) {
                $grades[] = new Grade($gradeId['PK_Grade']);
            }
            return $grades;
        }

        public function getGradeInModule($id_module)
        {
            $db = new Db();
            $gradeId = $db->getValue("SELECT PK_Grade FROM Grade WHERE FK_Student = $this->id AND FK_Module=$id_module");
            if ($gradeId)
                return new Grade($gradeId);
            else
                return false;
        }

        public function getGradesInModule($id_module)
        {
            $db = new Db();
            $gradesId = $db->query("SELECT PK_Grade FROM Grade WHERE FK_Student = $this->id AND FK_Module=$id_module");
            $grades = [];
            foreach ($gradesId as $gradeId) {
                $grades[] = new Grade($gradeId['PK_Grade']);
            }
            return $grades;
        }

        public function isInModule($id_module)
        {
            $db = new Db();
            return $db->getValue("SELECT 1 FROM Module_Person WHERE FK_Person = $this->id AND FK_Module = $id_module");
        }

        public function addToModule($id_module)
        {
            $db = new Db();
            $table = "Module_Person";
            $array_value = array(
                "FK_Person" => $this->id,
                "FK_Module" => $id_module);
            $db->insert($table, $array_value);
        }

        public function removeToModule($id_module)
        {
            $db = new Db();
            $table = "Module_Person";
            $array_value = array(
                "FK_Person" => $this->id,
                "FK_Module" => $id_module);
            $db->delete($table, $array_value);
        }

        public static function getAllUsers()
        {
            $db = new Db();
            $values = $db->query("SELECT DISTINCT Person.PK_Person
                                      FROM Person");
            $population = [];

            foreach ($values as $value) {
                $population[] = new Person($value['PK_Person']);
            }
            return $population;
        }

        /**
         * @return null
         */
        public function getFKPersonType()
        {
            return $this->FK_Person_Type;
        }

        /**
         * @param null $FK_Person_Type
         */
        public function setFKPersonType($FK_Person_Type)
        {
            $this->FK_Person_Type = $FK_Person_Type;
        }

        /**
         * @return null
         */
        public function getPersonLastname()
        {
            return $this->Person_Lastname;
        }

        /**
         * @param null $Person_Lastname
         */
        public function setPersonLastname($Person_Lastname)
        {
            $this->Person_Lastname = $Person_Lastname;
        }

        /**
         * @return null
         */
        public function getPersonFirstname()
        {
            return $this->Person_Firstname;
        }

        /**
         * @param null $Person_Firstname
         */
        public function setPersonFirstname($Person_Firstname)
        {
            $this->Person_Firstname = $Person_Firstname;
        }

        /**
         * @return null
         */
        public function getPersonMail()
        {
            return $this->Person_Mail;
        }

        /**
         * @param null $Person_Mail
         */
        public function setPersonMail($Person_Mail)
        {
            $this->Person_Mail = $Person_Mail;
        }

        /**
         * @return null
         */
        public function getPersonLogin()
        {
            return $this->Person_Login;
        }

        /**
         * @param null $Person_Login
         */
        public function setPersonLogin($Person_Login)
        {
            $this->Person_Login = $Person_Login;
        }

        /**
         * @return null
         */
        public function getPersonPassword()
        {
            return $this->Person_Password;
        }

        /**
         * @param null $Person_Password
         */
        public function setPersonPassword($Person_Password)
        {
            $this->Person_Password = $Person_Password;
        }

        
    }
}

?>