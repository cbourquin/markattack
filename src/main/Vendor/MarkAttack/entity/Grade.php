<?php
/**
 * Created by PhpStorm.
 * User: juju3
 * Date: 26/09/2018
 * Time: 14:22
 */

namespace MarkAttack\entity {

    require_once dirname(__FILE__) . '/ObjectModel.php';
    use MarkAttack\dao\Db;
    use PDO;

    class Grade extends ObjectModel
    {
        public $FK_Student;
        public $FK_Module;
        public $Grade_Value;

        public static $definition = array(
            'table' => 'Grade',
            'primary' => 'PK_Grade',
            'fields' => array(
                /* Classic fields */
                'FK_Student' => array('type' => self::TYPE_INT,'validate'=>'exist','field'=>array('table'=>'Person','column'=>'PK_Person')),
                'FK_Module' => array('type' => self::TYPE_INT,'validate'=>'exist','field'=>array('table'=>'Module','column'=>'PK_Module')),
                'Grade_Value' => array('type' => self::TYPE_FLOAT,'validate'=>'islimited', 'bornes' => array(
                    'options' => array( 'min_range' => 0,'max_range' => 20 ))
                )
            ),
        );

        public function __construct($id = null, $id_personne = null, $id_module = null, $note = null)
        {
            parent::__construct($id);
            if ($id_personne != null)
                $this->FK_Student = $id_personne;
            if ($id_module != null)
                $this->FK_Module = $id_module;
            if ($note != null)
                $this->Grade_Value = $note;
        }

        public function getModuleName()
        {
            $db = new Db();
            return $db->getValue("SELECT Module_Name FROM Module WHERE PK_Module = $this->FK_Module");
        }

        public function getModule()
        {
            return new Module($this->FK_Module);
        }

        public function getAllGradeOfModule(){
            $db = new Db();
            $sql = "SELECT Grade_Value
                    FROM Grade
                    WHERE FK_Module = :id ";
            return $db->query($sql,array(':id'=>$this->FK_Module),PDO::FETCH_COLUMN);
        }

        public function getGradeValue()
        {
            return $this->Grade_Value;
        }
        public function getFKStudent()
        {
            return $this->FK_Student;
        }

        public function setGradeValue($Grade_Value)
        {
            $this->Grade_Value = $Grade_Value;
        }

        public function getFKModule()
        {
            return $this->FK_Module;
        }
    }
}
?>