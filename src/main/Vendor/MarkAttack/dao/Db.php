<?php

namespace MarkAttack\dao{
    require_once dirname(__FILE__) . '/../error/DbException.php';
    use PDO;
    use MarkAttack\common\Tools;
    use MarkAttack\error\DbException;
    use MarkAttack\librairie\Page;
    use Exception;
    class Db
    {
        private $DB_HOST = "172.20.128.68";
        private $DB_USERNAME = "m2test1";
        private $DB_PASS = "m2test1";
        private $conn;

        public function __construct($db_name = 'm2test1')
        {

            if(array_key_exists('REQUEST_URI',$_SERVER)){
                if(preg_match("/\bpreprod\b/i",$_SERVER['REQUEST_URI'])){
                    $db_name = 'm2test1-test';
                }
            }
            try {
                $this->conn = new PDO("mysql:host=$this->DB_HOST;dbname=$db_name;charset=utf8", $this->DB_USERNAME, $this->DB_PASS);
            } catch (Exception $e) {
                Page::display('index.php?url=error-maintenance');
            }
        }

        public function getConnection()
        {
            return $this->conn;
        }

        /**
         * @param $table
         * @param $fields_to_insert Tableau clé valeur ou la clé correspond au nom de la colonne
         * @return string
         * @throws DbException
         */
        public function insert($table, $fields_to_insert,$add_quotes=false)
        {

            $sql = "INSERT INTO $table";
            $fieldsName = implode(',', array_keys($fields_to_insert));
            $sql .= "($fieldsName) VALUES (";
            $count = 0;
            foreach ($fields_to_insert as $field) {
                if ($count != 0)
                    $sql .= ",";
                if($add_quotes)
                    $sql .= "'".$field."'";
                else
                    $sql .= $field ;
                $count++;
            }
            $sql .= ")";
            if ($this->conn->exec($sql))
                return $this->conn->lastInsertId();
            else {
                $e = new DbException();
                $e->setMessage($this->conn->errorInfo());
                throw $e;
            }
        }

        public function update($table, $fields, $cond,$add_quotes = false)
        {

            $sql = 'UPDATE ' . Tools::escape($table) . ' SET ';
            foreach ($fields as $key => $value) {
                if (!is_array($value)) {
                    $value = array('type' => 'text', 'value' => $value);
                }
                if ($value['type'] == 'sql') {
                    if($add_quotes)
                        $sql .= ' ' . Tools::escape($key) . " = '{$value['value']}',";
                    else
                        $sql .= ' ' . Tools::escape($key) . " = {$value['value']},";
                } else {
                    if($add_quotes)
                        $sql .= (($value['value'] === '' || is_null($value['value']))) ? ' ' . Tools::escape($key) . ' = NULL, ' : ' ' . Tools::escape($key) . " = '{$value['value']}',";
                    else
                        $sql .= (($value['value'] === '' || is_null($value['value']))) ? ' ' . Tools::escape($key) . ' = NULL, ' : ' ' . Tools::escape($key) . " = {$value['value']},";
                }
            }

            $sql = rtrim($sql, ',');
            if ($cond) {
                $sql .= ' WHERE ' . $cond;
            }

            if ($this->conn->exec($sql)) {
                return true;
            } else {
                // print_r($this->conn->errorInfo());
                return false;
            }

        }

        public function query($sql, $params = null, $fetch = PDO::FETCH_ASSOC)
        {
            $statement = $this->conn->prepare($sql);
            if ($params) {
                $statement->execute($params);
            } else {
                $statement->execute();
            }
            if ($statement)
                return $statement->fetchAll($fetch);
            else
                return false;
        }

        public function getRow($sql)
        {
            //TO DO récupère une seul ligne de résultat
            $sql = rtrim($sql, " \t\n\r\0\x0B;") . ' LIMIT 1';
            $sth = $this->conn->prepare($sql);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);
            if ($result)
                return $result;
            return false;
        }

        public function getRowWithParams($sql, $params, $fetchStyle)
        {
            try {
                $stmt = $this->getConnection()->prepare($sql);
                $stmt->execute($params);
                return $stmt->fetch($fetchStyle);
            } catch (PDOException $e) {
                die('Erreur : ' . $e->getMessage());
            }
        }

        public function getTableWithParams($sql, $params)
        {
            try {
                $stmt = $this->getConnection()->prepare($sql);
                $stmt->execute($params);
                return $stmt->fetchAll();
            } catch (PDOException $e) {
                die('Erreur : ' . $e->getMessage());
            }
        }

        /**
         * @param $table_name
         * @param $conds Tableau de condition clé = colonne concerné, value valeur a tester
         * @return bool
         */
        public function delete($table_name, $conds)
        {
            try {
                $sql = "DELETE FROM $table_name";
                $count = 0;
                foreach ($conds as $key => $cond) {
                    if ($count != 0)
                        $sql .= " AND";
                    else
                        $sql .= " WHERE";
                    $sql .= " $key = $cond";
                    $count++;
                }
                $q = $this->conn->exec($sql);
                if($q !== 0 && $q)
                    return true;
                else
                    return false;
            }catch (PDOException $e){
                throw $e;
            }

        }


        public function getValue($sql,$params = null)
        {
            $statement = $this->conn->prepare($sql);
            if ($params) {
                $statement->execute($params);
            } else {
                $statement->execute();
            }

            if ($statement)
                return $statement->fetchColumn();
            else
                return false;
        }
    }
}
?>