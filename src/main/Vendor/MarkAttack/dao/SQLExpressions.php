<?php

final class SQLExpressions
{
    const SELECT_ALL_GRADES_OF_MODULE = "SELECT PK_Grade FROM Grade WHERE FK_Module = ?;";
    const SELECT_STUDENTS_OF_MODULE = "SELECT FK_Person FROM Module_Person WHERE FK_Module= ? AND FK_Person IN (SELECT PK_Person FROM Person WHERE FK_Person_Type = ?);";
    const SELECT_TEACHER_MODULE = "SELECT FK_Module FROM Module_Person WHERE FK_Person=?;";
}

?>