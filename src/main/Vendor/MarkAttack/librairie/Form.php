<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 02/10/2018
 * Time: 22:36
 */

namespace MarkAttack\librairie;

class Form
{
    public static function Authentification(){

        $options = array(
            'pseudo' => FILTER_SANITIZE_STRING,
            'pwd' => FILTER_SANITIZE_STRING,
        );

        $result = filter_input_array(INPUT_POST, $options);
        $nbErrs= 0;
        $errs = "";

        $msgEmptyErrs = array(
            'pseudo' => 'Merci de saisir un login dans le champ associé',
            'pwd' => 'Le champ mot de passe doit être rempli'
        );

        if (empty($_POST['pseudo']) and empty($_POST['pwd'])) {
            $errs = 'Les champs login et mot de passe doivent etre complétés';
            $nbErrs++;
        } else {
            foreach($options as $cle => $valeur) {
                if(empty($_POST[$cle])) {
                    $errs = $msgEmptyErrs[$cle];
                    $nbErrs++;
                    break;
                }
                elseif($result[$cle] === false) {
                    $errs = "Login ou mot de passe érroné";
                    $nbErrs++;
                    break;
                }
            }
        }

        if ($nbErrs){
            $_SESSION['errors'] = $errs;
            return false;
        }
        return $result;
    }

}