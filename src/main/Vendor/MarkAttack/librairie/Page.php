<?php
/**
 * Created by IntelliJ IDEA.
 * User: KELEKELE Ouziel
 * Date: 24/09/2018
 * Time: 23:24
 */

namespace MarkAttack\librairie;

class Page
{
    public static $vars = [];

    public static function set($key, $value){
        self::$vars[$key] = $value;
    }

    public static function get($key){
        return isset(self::$vars[$key])?self::$vars[$key]:false;
    }

    public static function display($route=''){
        session_start();
        if (isset($_SESSION['errors']))
            unset($_SESSION['errors']);
        header('Location: ' . $route);
        exit;
    }

    public static function displayErrs($route){
        header('Location: ' . $route);
        exit;
    }



}